<?php

namespace app\components;

use MongoDB\BSON\UTCDateTime;
use yii\di\Instance;
use yii\helpers\VarDumper;
use yii\log\Target;
use yii\mongodb\Connection;

class MongoDbTarget extends Target
{

    public $db = 'mongodb';

    public $logCollection = 'log';


    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::class);
    }

    public function export()
    {
        $rows = [];
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            if (!is_string($text)) {
                if ($text instanceof \Throwable || $text instanceof \Exception) {
                    $text = (string) $text;
                } else {
                    $text = VarDumper::export($text);
                }
            }
            $rows[] = [
                'level' => $level,
                'category' => $category,
                'log_time' => new UTCDateTime(time() * 1000),
                'prefix' => $this->getMessagePrefix($message),
                'message' => $text,
            ];
        }

        $this->db->getCollection($this->logCollection)->batchInsert($rows);
    }
}