<?php

namespace app\components;

use Dompdf\Exception;
use Yii;

class ConsoleController extends \yii\console\Controller
{

    protected $timestampStart;
    protected $deferredReload = 0;

    private $signal;

    public function init()
    {
        parent::init();
        $this->timestampStart = time();
        Yii::getLogger()->flushInterval = 1;
    }

    public function beforeAction($action)
    {
        if (\extension_loaded('pcntl')) {

            pcntl_signal(SIGHUP, function () {
                $this->signal = SIGHUP;
            });

            pcntl_signal(SIGINT, function () {
                $this->signal = SIGINT;
            });

            echo "SIGHUP, SIGINT handler registered\n";

        } else {
            Yii::warning('Extension pcntl is not installed');
        }

        return parent::beforeAction($action);
    }

    public function actionReload()
    {
        $memcache = $this->getCacheComponent();
        if ($memcache->set($this->getCacheTimestampStartKey(), time())) {
            echo "Shutdown request accepted.\n";
        }
    }

    /**
     * @param $secs
     * @throws Exception
     */
    public function wait($secs)
    {
        if ($secs > 0) {
            for ($i = 0; $i < $secs; $i++) {
                sleep(1);
                if ($this->isShuttingDown()) {
                    throw new Exception('Shutdown is requested');
                }
            }
        } else {
            if ($this->isShuttingDown()) {
                throw new Exception('Shutdown is requested');
            }
        }
    }

    protected function getCacheComponent()
    {
        return Yii::$app->cache;
    }


    public function isShuttingDown(): bool
    {
        if (\extension_loaded('pcntl')) {
            pcntl_signal_dispatch();
            if (\in_array($this->signal, [SIGINT, SIGHUP], true)) {
                return true;
            }
        }

        if ($this->timestampStart < $this->getCacheComponent()->get($this->getCacheTimestampStartKey())) {
            if (--$this->deferredReload < 0) {
                return true;
            }
            if ($this->deferredReload % 5 === 0) {
                echo "Shutdown after {$this->deferredReload} sec\n";
            }
        }

        return false;
    }

    public function getCacheTimestampStartKey(): string
    {
        return self::class . '-start-time';
    }

}