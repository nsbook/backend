<?php

namespace app\components;

use himiklab\sortablegrid\SortableGridBehavior as SortableGridB;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;


class SortableGridBehavior extends SortableGridB
{
    public function gridSort($items)
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if (!$model->hasAttribute($this->sortableAttribute)) {
            throw new InvalidConfigException("Model does not have sortable attribute `{$this->sortableAttribute}`.");
        }

        $newOrder = [];
        $models = [];
        foreach ($items as $old => $new) {
            $models[$new] = $model::findOne($new);
            $newOrder[$old] = $models[$new]->{$this->sortableAttribute} ? $models[$new]->{$this->sortableAttribute} : $new;
        }
        foreach ($newOrder as $modelId => $orderValue) {
            /** @var ActiveRecord[] $models */
            $models[$modelId]->updateAttributes([$this->sortableAttribute => $orderValue]);
        }

        if (is_callable($this->afterGridSort)) {
            call_user_func($this->afterGridSort, $model);
        }
    }

    public function beforeInsert()
    {

    }
}
