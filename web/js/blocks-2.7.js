function t121_setHeight(recid) {
    var div = $("#youtubeiframe" + recid);
    var height = div.width() * 0.5625;
    div.height(height);
    div.parent().height(height)
}

function t142_checkSize(recid) {
    var el = $("#rec" + recid).find(".t142__submit");
    if (el.length) {
        var btnheight = el.height() + 5;
        var textheight = el[0].scrollHeight;
        if (btnheight < textheight) {
            var btntext = el.text();
            el.addClass("t142__submit-overflowed");
            el.html("<span class=\"t142__text\">" + btntext + "</span>")
        }
    }
}

function t229_highlight(recid) {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t229__list_item a[href='" + url + "']").addClass("t-active");
    $(".t229__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t229__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t229__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t229__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t229__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t229_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t229_navLinks = $("#rec" + recid + " .t229__list_item a:not(.tooltipstered)[href*='#']");
        if (t229_navLinks.length > 0) {
            t229_catchScroll(t229_navLinks)
        }
    }
}

function t229_catchScroll(t229_navLinks) {
    var t229_clickedSectionId = null, t229_sections = new Array(), t229_sectionIdTonavigationLink = [],
        t229_interval = 100, t229_lastCall, t229_timeoutId;
    t229_navLinks = $(t229_navLinks.get().reverse());
    t229_navLinks.each(function () {
        var t229_cursection = t229_getSectionByHref($(this));
        if (typeof t229_cursection.attr("id") != "undefined") {
            t229_sections.push(t229_cursection)
        }
        t229_sectionIdTonavigationLink[t229_cursection.attr("id")] = $(this)
    });
    t229_updateSectionsOffsets(t229_sections);
    t229_sections.sort(function (a, b) {
        return b.attr("data-offset-top") - a.attr("data-offset-top")
    });
    $(window).bind('resize', t_throttle(function () {
        t229_updateSectionsOffsets(t229_sections)
    }, 200));
    $('.t229').bind('displayChanged', function () {
        t229_updateSectionsOffsets(t229_sections)
    });
    setInterval(function () {
        t229_updateSectionsOffsets(t229_sections)
    }, 5000);
    t229_highlightNavLinks(t229_navLinks, t229_sections, t229_sectionIdTonavigationLink, t229_clickedSectionId);
    t229_navLinks.click(function () {
        var t229_clickedSection = t229_getSectionByHref($(this));
        if (!$(this).hasClass("tooltipstered") && typeof t229_clickedSection.attr("id") != "undefined") {
            t229_navLinks.removeClass('t-active');
            $(this).addClass('t-active');
            t229_clickedSectionId = t229_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t229_now = new Date().getTime();
        if (t229_lastCall && t229_now < (t229_lastCall + t229_interval)) {
            clearTimeout(t229_timeoutId);
            t229_timeoutId = setTimeout(function () {
                t229_lastCall = t229_now;
                t229_clickedSectionId = t229_highlightNavLinks(t229_navLinks, t229_sections, t229_sectionIdTonavigationLink, t229_clickedSectionId)
            }, t229_interval - (t229_now - t229_lastCall))
        } else {
            t229_lastCall = t229_now;
            t229_clickedSectionId = t229_highlightNavLinks(t229_navLinks, t229_sections, t229_sectionIdTonavigationLink, t229_clickedSectionId)
        }
    })
}

function t229_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t229_curSection = $(this);
        t229_curSection.attr("data-offset-top", t229_curSection.offset().top)
    })
}

function t229_getSectionByHref(curlink) {
    var t229_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (t229_curLinkValue[0] == '/') {
        t229_curLinkValue = t229_curLinkValue.substring(1)
    }
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t229_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t229_curLinkValue.substring(1) + "']")
    }
}

function t229_highlightNavLinks(t229_navLinks, t229_sections, t229_sectionIdTonavigationLink, t229_clickedSectionId) {
    var t229_scrollPosition = $(window).scrollTop(), t229_valueToReturn = t229_clickedSectionId;
    if (t229_sections.length != 0 && t229_clickedSectionId == null && t229_sections[t229_sections.length - 1].attr("data-offset-top") > (t229_scrollPosition + 300)) {
        t229_navLinks.removeClass('t-active');
        return null
    }
    $(t229_sections).each(function (e) {
        var t229_curSection = $(this), t229_sectionTop = t229_curSection.attr("data-offset-top"),
            t229_id = t229_curSection.attr('id'), t229_navLink = t229_sectionIdTonavigationLink[t229_id];
        if (((t229_scrollPosition + 300) >= t229_sectionTop) || (t229_sections[0].attr("id") == t229_id && t229_scrollPosition >= $(document).height() - $(window).height())) {
            if (t229_clickedSectionId == null && !t229_navLink.hasClass('t-active')) {
                t229_navLinks.removeClass('t-active');
                t229_navLink.addClass('t-active');
                t229_valueToReturn = null
            } else {
                if (t229_clickedSectionId != null && t229_id == t229_clickedSectionId) {
                    t229_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t229_valueToReturn
}

function t229_setPath(pageid) {
}

function t229_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t229").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t229").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t229_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $("#rec" + recid + " .t229").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-50px");
                        el.css("visibility", "visible");
                        el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                        })
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden")
                }
            }
        })
    }
}

function t229_changeBgOpacityMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t229").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll)
            } else {
                el.css("background-color", bgcolor)
            }
        })
    }
}

function t300_init() {
    $(".t300").each(function () {
        var $hook = $(this).attr('data-tooltip-hook'), $recid = $(this).attr('data-tooltip-id');
        if ($hook !== '') {
            var $obj = $('a[href*="' + $hook + '"]');
            var $content = $(this).find('.t300__content').html();
            if ($hook.charAt(0) == '#') {
                var touchDevices = !0
            } else {
                var touchDevices = !1
            }
            var position = $(this).attr('data-tooltip-position');
            if (position !== '') {
            } else {
                position = 'top'
            }
            $obj.tooltipster({
                'theme': 't300__tooltipster-noir t300__tooltipster-noir_' + $recid + '',
                'contentAsHTML': !0,
                'content': $content,
                interactive: !0,
                touchDevices: touchDevices,
                position: position
            })
        }
    })
}

$(document).ready(function () {
    t300_init();
    setTimeout(function () {
        t300_init()
    }, 500)
});

function t390_showPopup(recid) {
    var el = $('#rec' + recid), popup = el.find('.t-popup');
    popup.css('display', 'block');
    setTimeout(function () {
        popup.find('.t-popup__container').addClass('t-popup__container-animated');
        popup.addClass('t-popup_show')
    }, 50);
    $('body').addClass('t-body_popupshowed');
    el.find('.t-popup').click(function (e) {
        if (e.target == this) {
            t390_closePopup()
        }
    });
    el.find('.t-popup__close').click(function (e) {
        t390_closePopup()
    });
    el.find('a[href*=#]').click(function (e) {
        var url = $(this).attr('href');
        if (!url || url.substring(0, 7) != '#price:') {
            t390_closePopup();
            if (!url || url.substring(0, 7) == '#popup:') {
                setTimeout(function () {
                    $('body').addClass('t-body_popupshowed')
                }, 300)
            }
        }
    });
    $(document).keydown(function (e) {
        if (e.keyCode == 27) {
            t390_closePopup()
        }
    })
}

function t390_closePopup() {
    $('body').removeClass('t-body_popupshowed');
    $('.t-popup').removeClass('t-popup_show');
    setTimeout(function () {
        $('.t-popup').not('.t-popup_show').css('display', 'none')
    }, 300)
}

function t390_resizePopup(recid) {
    var el = $("#rec" + recid), div = el.find(".t-popup__container").height(), win = $(window).height() - 120,
        popup = el.find(".t-popup__container");
    if (div > win) {
        popup.addClass('t-popup__container-static')
    } else {
        popup.removeClass('t-popup__container-static')
    }
}

function t390_sendPopupEventToStatistics(popupname) {
    var virtPage = '/tilda/popup/';
    var virtTitle = 'Popup: ';
    if (popupname.substring(0, 7) == '#popup:') {
        popupname = popupname.substring(7)
    }
    virtPage += popupname;
    virtTitle += popupname;
    if (window.Tilda && typeof Tilda.sendEventToStatistics == 'function') {
        Tilda.sendEventToStatistics(virtPage, virtTitle, '', 0)
    } else {
        if (ga) {
            if (window.mainTracker != 'tilda') {
                ga('send', {'hitType': 'pageview', 'page': virtPage, 'title': virtTitle})
            }
        }
        if (window.mainMetrika > '' && window[window.mainMetrika]) {
            window[window.mainMetrika].hit(virtPage, {title: virtTitle, referer: window.location.href})
        }
    }
}

function t390_initPopup(recid) {
    $('#rec' + recid).attr('data-animationappear', 'off');
    $('#rec' + recid).css('opacity', '1');
    var el = $('#rec' + recid).find('.t-popup'), hook = el.attr('data-tooltip-hook'),
        analitics = el.attr('data-track-popup');
    if (hook !== '') {
        var obj = $('a[href="' + hook + '"]');
        obj.click(function (e) {
            t390_showPopup(recid);
            t390_resizePopup(recid);
            e.preventDefault();
            if (window.lazy == 'y') {
                t_lazyload_update()
            }
            if (analitics > '') {
                var virtTitle = hook;
                if (virtTitle.substring(0, 7) == '#popup:') {
                    virtTitle = virtTitle.substring(7)
                }
                Tilda.sendEventToStatistics(analitics, virtTitle)
            }
        })
    }
}

function t396_init(recid) {
    var data = '';
    var res = t396_detectResolution();
    t396_initTNobj();
    t396_switchResolution(res);
    t396_updateTNobj();
    t396_artboard_build(data, recid);
    window.tn_window_width = $(window).width();
    $(window).resize(function () {
        tn_console('>>>> t396: Window on Resize event >>>>');
        t396_waitForFinalEvent(function () {
            if ($isMobile) {
                var ww = $(window).width();
                if (ww != window.tn_window_width) {
                    t396_doResize(recid)
                }
            } else {
                t396_doResize(recid)
            }
        }, 500, 'resizeruniqueid' + recid)
    });
    $(window).on("orientationchange", function () {
        tn_console('>>>> t396: Orient change event >>>>');
        t396_waitForFinalEvent(function () {
            t396_doResize(recid)
        }, 600, 'orientationuniqueid' + recid)
    });
    $(window).load(function () {
        var ab = $('#rec' + recid).find('.t396__artboard');
        t396_allelems__renderView(ab)
    })
}

function t396_doResize(recid) {
    var ww = $(window).width();
    window.tn_window_width = ww;
    var res = t396_detectResolution();
    var ab = $('#rec' + recid).find('.t396__artboard');
    t396_switchResolution(res);
    t396_updateTNobj();
    t396_ab__renderView(ab);
    t396_allelems__renderView(ab)
}

function t396_detectResolution() {
    var ww = $(window).width();
    var res;
    res = 1200;
    if (ww < 1200) {
        res = 960
    }
    if (ww < 960) {
        res = 640
    }
    if (ww < 640) {
        res = 480
    }
    if (ww < 480) {
        res = 320
    }
    return (res)
}

function t396_initTNobj() {
    tn_console('func: initTNobj');
    window.tn = {};
    window.tn.canvas_min_sizes = ["320", "480", "640", "960", "1200"];
    window.tn.canvas_max_sizes = ["480", "640", "960", "1200", ""];
    window.tn.ab_fields = ["height", "width", "bgcolor", "bgimg", "bgattachment", "bgposition", "filteropacity", "filtercolor", "filteropacity2", "filtercolor2", "height_vh", "valign"]
}

function t396_updateTNobj() {
    tn_console('func: updateTNobj');
    if (typeof window.zero_window_width_hook != 'undefined' && window.zero_window_width_hook == 'allrecords' && $('#allrecords').length) {
        window.tn.window_width = parseInt($('#allrecords').width())
    } else {
        window.tn.window_width = parseInt($(window).width())
    }
    window.tn.window_height = parseInt($(window).height());
    if (window.tn.curResolution == 1200) {
        window.tn.canvas_min_width = 1200;
        window.tn.canvas_max_width = window.tn.window_width
    }
    if (window.tn.curResolution == 960) {
        window.tn.canvas_min_width = 960;
        window.tn.canvas_max_width = 1200
    }
    if (window.tn.curResolution == 640) {
        window.tn.canvas_min_width = 640;
        window.tn.canvas_max_width = 960
    }
    if (window.tn.curResolution == 480) {
        window.tn.canvas_min_width = 480;
        window.tn.canvas_max_width = 640
    }
    if (window.tn.curResolution == 320) {
        window.tn.canvas_min_width = 320;
        window.tn.canvas_max_width = 480
    }
    window.tn.grid_width = window.tn.canvas_min_width;
    window.tn.grid_offset_left = parseFloat((window.tn.window_width - window.tn.grid_width) / 2)
}

var t396_waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId"
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId])
        }
        timers[uniqueId] = setTimeout(callback, ms)
    }
})();

function t396_switchResolution(res, resmax) {
    tn_console('func: switchResolution');
    if (typeof resmax == 'undefined') {
        if (res == 1200) resmax = '';
        if (res == 960) resmax = 1200;
        if (res == 640) resmax = 960;
        if (res == 480) resmax = 640;
        if (res == 320) resmax = 480
    }
    window.tn.curResolution = res;
    window.tn.curResolution_max = resmax
}

function t396_artboard_build(data, recid) {
    tn_console('func: t396_artboard_build. Recid:' + recid);
    tn_console(data);
    var ab = $('#rec' + recid).find('.t396__artboard');
    t396_ab__renderView(ab);
    ab.find('.tn-elem').each(function () {
        var item = $(this);
        if (item.attr('data-elem-type') == 'text') {
            t396_addText(ab, item)
        }
        if (item.attr('data-elem-type') == 'image') {
            t396_addImage(ab, item)
        }
        if (item.attr('data-elem-type') == 'shape') {
            t396_addShape(ab, item)
        }
        if (item.attr('data-elem-type') == 'button') {
            t396_addButton(ab, item)
        }
        if (item.attr('data-elem-type') == 'video') {
            t396_addVideo(ab, item)
        }
        if (item.attr('data-elem-type') == 'html') {
            t396_addHtml(ab, item)
        }
        if (item.attr('data-elem-type') == 'tooltip') {
            t396_addTooltip(ab, item)
        }
    });
    $('#rec' + recid).find('.t396__artboard').removeClass('rendering').addClass('rendered')
}

function t396_ab__renderView(ab) {
    var fields = window.tn.ab_fields;
    for (var i = 0; i < fields.length; i++) {
        t396_ab__renderViewOneField(ab, fields[i])
    }
    var ab_min_height = t396_ab__getFieldValue(ab, 'height');
    var ab_max_height = t396_ab__getHeight(ab);
    var offset_top = 0;
    if (ab_min_height == ab_max_height) {
        offset_top = 0
    } else {
        var ab_valign = t396_ab__getFieldValue(ab, 'valign');
        if (ab_valign == 'top') {
            offset_top = 0
        } else if (ab_valign == 'center') {
            offset_top = parseFloat((ab_max_height - ab_min_height) / 2).toFixed(1)
        } else if (ab_valign == 'bottom') {
            offset_top = parseFloat((ab_max_height - ab_min_height)).toFixed(1)
        } else if (ab_valign == 'stretch') {
            offset_top = 0;
            ab_min_height = ab_max_height
        } else {
            offset_top = 0
        }
    }
    ab.attr('data-artboard-proxy-min-offset-top', offset_top);
    ab.attr('data-artboard-proxy-min-height', ab_min_height);
    ab.attr('data-artboard-proxy-max-height', ab_max_height)
}

function t396_addText(ab, el) {
    tn_console('func: addText');
    var fields_str = 'top,left,width,container,axisx,axisy,widthunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addImage(ab, el) {
    tn_console('func: addImage');
    var fields_str = 'img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    el.find('img').on("load", function () {
        t396_elem__renderViewOneField(el, 'top');
        if (typeof $(this).attr('src') != 'undefined' && $(this).attr('src') != '') {
            setTimeout(function () {
                t396_elem__renderViewOneField(el, 'top')
            }, 2000)
        }
    }).each(function () {
        if (this.complete) $(this).load()
    });
    el.find('img').on('tuwidget_done', function (e, file) {
        t396_elem__renderViewOneField(el, 'top')
    })
}

function t396_addShape(ab, el) {
    tn_console('func: addShape');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addButton(ab, el) {
    tn_console('func: addButton');
    var fields_str = 'top,left,width,height,container,axisx,axisy,caption,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    return (el)
}

function t396_addVideo(ab, el) {
    tn_console('func: addVideo');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    var viel = el.find('.tn-atom__videoiframe');
    var viatel = el.find('.tn-atom');
    viatel.css('background-color', '#000');
    var vihascover = viatel.attr('data-atom-video-has-cover');
    if (typeof vihascover == 'undefined') {
        vihascover = ''
    }
    if (vihascover == 'y') {
        viatel.click(function () {
            var viifel = viel.find('iframe');
            if (viifel.length) {
                var foo = viifel.attr('data-original');
                viifel.attr('src', foo)
            }
            viatel.css('background-image', 'none');
            viatel.find('.tn-atom__video-play-link').css('display', 'none')
        })
    }
    var autoplay = t396_elem__getFieldValue(el, 'autoplay');
    var showinfo = t396_elem__getFieldValue(el, 'showinfo');
    var loop = t396_elem__getFieldValue(el, 'loop');
    var mute = t396_elem__getFieldValue(el, 'mute');
    var startsec = t396_elem__getFieldValue(el, 'startsec');
    var endsec = t396_elem__getFieldValue(el, 'endsec');
    var tmode = $('#allrecords').attr('data-tilda-mode');
    var url = '';
    var viyid = viel.attr('data-youtubeid');
    if (typeof viyid != 'undefined' && viyid != '') {
        url = '//www.youtube.com/embed/';
        url += viyid + '?rel=0&fmt=18&html5=1';
        url += '&showinfo=' + (showinfo == 'y' ? '1' : '0');
        if (loop == 'y') {
            url += '&loop=1&playlist=' + viyid
        }
        if (startsec > 0) {
            url += '&start=' + startsec
        }
        if (endsec > 0) {
            url += '&end=' + endsec
        }
        if (vihascover == 'y') {
            url += '&autoplay=1';
            viel.html('<iframe id="youtubeiframe" width="100%" height="100%" data-original="' + url + '" frameborder="0" allowfullscreen data-flag-inst="y"></iframe>')
        } else {
            if (typeof tmode != 'undefined' && tmode == 'edit') {
            } else {
                if (autoplay == 'y') {
                    url += '&autoplay=1'
                }
            }
            if (window.lazy == 'y') {
                viel.html('<iframe id="youtubeiframe" class="t-iframe" width="100%" height="100%" data-original="' + url + '" frameborder="0" allowfullscreen data-flag-inst="lazy"></iframe>');
                el.append('<script>lazyload_iframe = new LazyLoad({elements_selector: ".t-iframe"});</script>')
            } else {
                viel.html('<iframe id="youtubeiframe" width="100%" height="100%" src="' + url + '" frameborder="0" allowfullscreen data-flag-inst="y"></iframe>')
            }
        }
    }
    var vivid = viel.attr('data-vimeoid');
    if (typeof vivid != 'undefined' && vivid > 0) {
        url = '//player.vimeo.com/video/';
        url += vivid + '?color=ffffff&badge=0';
        if (showinfo == 'y') {
            url += '&title=1&byline=1&portrait=1'
        } else {
            url += '&title=0&byline=0&portrait=0'
        }
        if (loop == 'y') {
            url += '&loop=1'
        }
        if (vihascover == 'y') {
            url += '&autoplay=1';
            viel.html('<iframe class="t-iframe" data-original="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
        } else {
            if (typeof tmode != 'undefined' && tmode == 'edit') {
            } else {
                if (autoplay == 'y') {
                    url += '&autoplay=1'
                }
            }
            if (window.lazy == 'y') {
                viel.html('<iframe class="t-iframe" data-original="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                el.append('<script>lazyload_iframe = new LazyLoad({elements_selector: ".t-iframe"});</script>')
            } else {
                viel.html('<iframe src="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
            }
        }
    }
}

function t396_addHtml(ab, el) {
    tn_console('func: addHtml');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addTooltip(ab, el) {
    tn_console('func: addTooltip');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits,tipposition';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    var pinEl = el.find('.tn-atom__pin');
    var tipEl = el.find('.tn-atom__tip');
    var tipopen = el.attr('data-field-tipopen-value');
    if (isMobile || (typeof tipopen != 'undefined' && tipopen == 'click')) {
        t396_setUpTooltip_mobile(el, pinEl, tipEl)
    } else {
        t396_setUpTooltip_desktop(el, pinEl, tipEl)
    }
    setTimeout(function () {
        $('.tn-atom__tip-img').each(function () {
            var foo = $(this).attr('data-tipimg-original');
            if (typeof foo != 'undefined' && foo != '') {
                $(this).attr('src', foo)
            }
        })
    }, 3000)
}

function t396_elem__setFieldValue(el, prop, val, flag_render, flag_updateui, res) {
    if (res == '') res = window.tn.curResolution;
    if (res < 1200 && prop != 'zindex') {
        el.attr('data-field-' + prop + '-res-' + res + '-value', val)
    } else {
        el.attr('data-field-' + prop + '-value', val)
    }
    if (flag_render == 'render') elem__renderViewOneField(el, prop);
    if (flag_updateui == 'updateui') panelSettings__updateUi(el, prop, val)
}

function t396_elem__getFieldValue(el, prop) {
    var res = window.tn.curResolution;
    var r;
    if (res < 1200) {
        if (res == 960) {
            r = el.attr('data-field-' + prop + '-res-960-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-value')
            }
        }
        if (res == 640) {
            r = el.attr('data-field-' + prop + '-res-640-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-960-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-value')
                }
            }
        }
        if (res == 480) {
            r = el.attr('data-field-' + prop + '-res-480-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-640-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-res-960-value');
                    if (typeof r == 'undefined') {
                        r = el.attr('data-field-' + prop + '-value')
                    }
                }
            }
        }
        if (res == 320) {
            r = el.attr('data-field-' + prop + '-res-320-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-480-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-res-640-value');
                    if (typeof r == 'undefined') {
                        r = el.attr('data-field-' + prop + '-res-960-value');
                        if (typeof r == 'undefined') {
                            r = el.attr('data-field-' + prop + '-value')
                        }
                    }
                }
            }
        }
    } else {
        r = el.attr('data-field-' + prop + '-value')
    }
    return (r)
}

function t396_elem__renderView(el) {
    tn_console('func: elem__renderView');
    var fields = el.attr('data-fields');
    if (!fields) {
        return !1
    }
    fields = fields.split(',');
    for (var i = 0; i < fields.length; i++) {
        t396_elem__renderViewOneField(el, fields[i])
    }
}

function t396_elem__renderViewOneField(el, field) {
    var value = t396_elem__getFieldValue(el, field);
    if (field == 'left') {
        value = t396_elem__convertPosition__Local__toAbsolute(el, field, value);
        el.css('left', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'top') {
        value = t396_elem__convertPosition__Local__toAbsolute(el, field, value);
        el.css('top', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'width') {
        value = t396_elem__getWidth(el, value);
        el.css('width', parseFloat(value).toFixed(1) + 'px');
        var eltype = el.attr('data-elem-type');
        if (eltype == 'tooltip') {
            el.css('height', parseInt(value).toFixed(1) + 'px')
        }
    }
    if (field == 'height') {
        value = t396_elem__getHeight(el, value);
        el.css('height', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'container') {
        t396_elem__renderViewOneField(el, 'left');
        t396_elem__renderViewOneField(el, 'top')
    }
    if (field == 'width' || field == 'height' || field == 'fontsize' || field == 'fontfamily' || field == 'letterspacing' || field == 'fontweight' || field == 'img') {
        t396_elem__renderViewOneField(el, 'left');
        t396_elem__renderViewOneField(el, 'top')
    }
}

function t396_elem__convertPosition__Local__toAbsolute(el, field, value) {
    value = parseInt(value);
    if (field == 'left') {
        var el_container, offset_left, el_container_width, el_width;
        var container = t396_elem__getFieldValue(el, 'container');
        if (container == 'grid') {
            el_container = 'grid';
            offset_left = window.tn.grid_offset_left;
            el_container_width = window.tn.grid_width
        } else {
            el_container = 'window';
            offset_left = 0;
            el_container_width = window.tn.window_width
        }
        var el_leftunits = t396_elem__getFieldValue(el, 'leftunits');
        if (el_leftunits == '%') {
            value = t396_roundFloat(el_container_width * value / 100)
        }
        value = offset_left + value;
        var el_axisx = t396_elem__getFieldValue(el, 'axisx');
        if (el_axisx == 'center') {
            el_width = t396_elem__getWidth(el);
            value = el_container_width / 2 - el_width / 2 + value
        }
        if (el_axisx == 'right') {
            el_width = t396_elem__getWidth(el);
            value = el_container_width - el_width + value
        }
    }
    if (field == 'top') {
        var ab = el.parent();
        var el_container, offset_top, el_container_height, el_height;
        var container = t396_elem__getFieldValue(el, 'container');
        if (container == 'grid') {
            el_container = 'grid';
            offset_top = parseFloat(ab.attr('data-artboard-proxy-min-offset-top'));
            el_container_height = parseFloat(ab.attr('data-artboard-proxy-min-height'))
        } else {
            el_container = 'window';
            offset_top = 0;
            el_container_height = parseFloat(ab.attr('data-artboard-proxy-max-height'))
        }
        var el_topunits = t396_elem__getFieldValue(el, 'topunits');
        if (el_topunits == '%') {
            value = (el_container_height * (value / 100))
        }
        value = offset_top + value;
        var el_axisy = t396_elem__getFieldValue(el, 'axisy');
        if (el_axisy == 'center') {
            el_height = t396_elem__getHeight(el);
            value = el_container_height / 2 - el_height / 2 + value
        }
        if (el_axisy == 'bottom') {
            el_height = t396_elem__getHeight(el);
            value = el_container_height - el_height + value
        }
    }
    return (value)
}

function t396_ab__setFieldValue(ab, prop, val, res) {
    if (res == '') res = window.tn.curResolution;
    if (res < 1200) {
        ab.attr('data-artboard-' + prop + '-res-' + res, val)
    } else {
        ab.attr('data-artboard-' + prop, val)
    }
}

function t396_ab__getFieldValue(ab, prop) {
    var res = window.tn.curResolution;
    var r;
    if (res < 1200) {
        if (res == 960) {
            r = ab.attr('data-artboard-' + prop + '-res-960');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '')
            }
        }
        if (res == 640) {
            r = ab.attr('data-artboard-' + prop + '-res-640');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-960');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '')
                }
            }
        }
        if (res == 480) {
            r = ab.attr('data-artboard-' + prop + '-res-480');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-640');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '-res-960');
                    if (typeof r == 'undefined') {
                        r = ab.attr('data-artboard-' + prop + '')
                    }
                }
            }
        }
        if (res == 320) {
            r = ab.attr('data-artboard-' + prop + '-res-320');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-480');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '-res-640');
                    if (typeof r == 'undefined') {
                        r = ab.attr('data-artboard-' + prop + '-res-960');
                        if (typeof r == 'undefined') {
                            r = ab.attr('data-artboard-' + prop + '')
                        }
                    }
                }
            }
        }
    } else {
        r = ab.attr('data-artboard-' + prop)
    }
    return (r)
}

function t396_ab__renderViewOneField(ab, field) {
    var value = t396_ab__getFieldValue(ab, field)
}

function t396_allelems__renderView(ab) {
    tn_console('func: allelems__renderView: abid:' + ab.attr('data-artboard-recid'));
    ab.find(".tn-elem").each(function () {
        t396_elem__renderView($(this))
    })
}

function t396_ab__filterUpdate(ab) {
    var filter = ab.find('.t396__filter');
    var c1 = filter.attr('data-filtercolor-rgb');
    var c2 = filter.attr('data-filtercolor2-rgb');
    var o1 = filter.attr('data-filteropacity');
    var o2 = filter.attr('data-filteropacity2');
    if ((typeof c2 == 'undefined' || c2 == '') && (typeof c1 != 'undefined' && c1 != '')) {
        filter.css("background-color", "rgba(" + c1 + "," + o1 + ")")
    } else if ((typeof c1 == 'undefined' || c1 == '') && (typeof c2 != 'undefined' && c2 != '')) {
        filter.css("background-color", "rgba(" + c2 + "," + o2 + ")")
    } else if (typeof c1 != 'undefined' && typeof c2 != 'undefined' && c1 != '' && c2 != '') {
        filter.css({background: "-webkit-gradient(linear, left top, left bottom, from(rgba(" + c1 + "," + o1 + ")), to(rgba(" + c2 + "," + o2 + ")) )"})
    } else {
        filter.css("background-color", 'transparent')
    }
}

function t396_ab__getHeight(ab, ab_height) {
    if (typeof ab_height == 'undefined') ab_height = t396_ab__getFieldValue(ab, 'height');
    ab_height = parseFloat(ab_height);
    var ab_height_vh = t396_ab__getFieldValue(ab, 'height_vh');
    if (ab_height_vh != '') {
        ab_height_vh = parseFloat(ab_height_vh);
        if (isNaN(ab_height_vh) === !1) {
            var ab_height_vh_px = parseFloat(window.tn.window_height * parseFloat(ab_height_vh / 100));
            if (ab_height < ab_height_vh_px) {
                ab_height = ab_height_vh_px
            }
        }
    }
    return (ab_height)
}

function t396_hex2rgb(hexStr) {
    var hex = parseInt(hexStr.substring(1), 16);
    var r = (hex & 0xff0000) >> 16;
    var g = (hex & 0x00ff00) >> 8;
    var b = hex & 0x0000ff;
    return [r, g, b]
}

String.prototype.t396_replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement)
};

function t396_elem__getWidth(el, value) {
    if (typeof value == 'undefined') value = parseFloat(t396_elem__getFieldValue(el, 'width'));
    var el_widthunits = t396_elem__getFieldValue(el, 'widthunits');
    if (el_widthunits == '%') {
        var el_container = t396_elem__getFieldValue(el, 'container');
        if (el_container == 'window') {
            value = parseFloat(window.tn.window_width * parseFloat(parseInt(value) / 100))
        } else {
            value = parseFloat(window.tn.grid_width * parseFloat(parseInt(value) / 100))
        }
    }
    return (value)
}

function t396_elem__getHeight(el, value) {
    if (typeof value == 'undefined') value = t396_elem__getFieldValue(el, 'height');
    value = parseFloat(value);
    if (el.attr('data-elem-type') == 'shape' || el.attr('data-elem-type') == 'video' || el.attr('data-elem-type') == 'html') {
        var el_heightunits = t396_elem__getFieldValue(el, 'heightunits');
        if (el_heightunits == '%') {
            var ab = el.parent();
            var ab_min_height = parseFloat(ab.attr('data-artboard-proxy-min-height'));
            var ab_max_height = parseFloat(ab.attr('data-artboard-proxy-max-height'));
            var el_container = t396_elem__getFieldValue(el, 'container');
            if (el_container == 'window') {
                value = parseFloat(ab_max_height * parseFloat(value / 100))
            } else {
                value = parseFloat(ab_min_height * parseFloat(value / 100))
            }
        }
    } else if (el.attr('data-elem-type') == 'button') {
        value = value
    } else {
        value = parseFloat(el.innerHeight())
    }
    return (value)
}

function t396_roundFloat(n) {
    n = Math.round(n * 100) / 100;
    return (n)
}

function tn_console(str) {
    if (window.tn_comments == 1) console.log(str)
}

function t396_setUpTooltip_desktop(el, pinEl, tipEl) {
    var timer;
    pinEl.mouseover(function () {
        $('.tn-atom__tip_visible').each(function () {
            var thisTipEl = $(this).parents('.t396__elem');
            if (thisTipEl.attr('data-elem-id') != el.attr('data-elem-id')) {
                t396_hideTooltip(thisTipEl, $(this))
            }
        });
        clearTimeout(timer);
        if (tipEl.css('display') == 'block') {
            return
        }
        t396_showTooltip(el, tipEl)
    });
    pinEl.mouseout(function () {
        timer = setTimeout(function () {
            t396_hideTooltip(el, tipEl)
        }, 300)
    })
}

function t396_setUpTooltip_mobile(el, pinEl, tipEl) {
    pinEl.on('click', function (e) {
        if (tipEl.css('display') == 'block' && $(e.target).hasClass("tn-atom__pin")) {
            t396_hideTooltip(el, tipEl)
        } else {
            t396_showTooltip(el, tipEl)
        }
    });
    var id = el.attr("data-elem-id");
    $(document).click(function (e) {
        var isInsideTooltip = ($(e.target).hasClass("tn-atom__pin") || $(e.target).parents(".tn-atom__pin").length > 0);
        if (isInsideTooltip) {
            var clickedPinId = $(e.target).parents(".t396__elem").attr("data-elem-id");
            if (clickedPinId == id) {
                return
            }
        }
        t396_hideTooltip(el, tipEl)
    })
}

function t396_hideTooltip(el, tipEl) {
    tipEl.css('display', '');
    tipEl.css({"left": "", "transform": "", "right": ""});
    tipEl.removeClass('tn-atom__tip_visible');
    el.css('z-index', '')
}

function t396_showTooltip(el, tipEl) {
    var pos = el.attr("data-field-tipposition-value");
    if (typeof pos == 'undefined' || pos == '') {
        pos = 'top'
    }
    ;var elSize = el.height();
    var elTop = el.offset().top;
    var elBottom = elTop + elSize;
    var elLeft = el.offset().left;
    var elRight = el.offset().left + elSize;
    var winTop = $(window).scrollTop();
    var winWidth = $(window).width();
    var winBottom = winTop + $(window).height();
    var tipElHeight = tipEl.outerHeight();
    var tipElWidth = tipEl.outerWidth();
    var padd = 15;
    if (pos == 'right' || pos == 'left') {
        var tipElRight = elRight + padd + tipElWidth;
        var tipElLeft = elLeft - padd - tipElWidth;
        if ((pos == 'right' && tipElRight > winWidth) || (pos == 'left' && tipElLeft < 0)) {
            pos = 'top'
        }
    }
    if (pos == 'top' || pos == 'bottom') {
        var tipElRight = elRight + (tipElWidth / 2 - elSize / 2);
        var tipElLeft = elLeft - (tipElWidth / 2 - elSize / 2);
        if (tipElRight > winWidth) {
            var rightOffset = -(winWidth - elRight - padd);
            tipEl.css({"left": "auto", "transform": "none", "right": rightOffset + "px"})
        }
        if (tipElLeft < 0) {
            var leftOffset = -(elLeft - padd);
            tipEl.css({"left": leftOffset + "px", "transform": "none"})
        }
    }
    if (pos == 'top') {
        var tipElTop = elTop - padd - tipElHeight;
        if (winTop > tipElTop) {
            pos = 'bottom'
        }
    }
    if (pos == 'bottom') {
        var tipElBottom = elBottom + padd + tipElHeight;
        if (winBottom < tipElBottom) {
            pos = 'top'
        }
    }
    tipEl.attr('data-tip-pos', pos);
    tipEl.css('display', 'block');
    tipEl.addClass('tn-atom__tip_visible');
    el.css('z-index', '1000')
}

function t454_setLogoPadding(recid) {
    if ($(window).width() > 980) {
        var t454__menu = $('#rec' + recid + ' .t454');
        var t454__logo = t454__menu.find('.t454__logowrapper');
        var t454__leftpart = t454__menu.find('.t454__leftwrapper');
        var t454__rightpart = t454__menu.find('.t454__rightwrapper');
        t454__leftpart.css("padding-right", t454__logo.width() / 2 + 50);
        t454__rightpart.css("padding-left", t454__logo.width() / 2 + 50)
    }
}

function t454_highlight() {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t454__list_item a[href='" + url + "']").addClass("t-active");
    $(".t454__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t454__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t454__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t454__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t454__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t454_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t454_navLinks = $("#rec" + recid + " .t454__list_item a:not(.tooltipstered)[href*='#']");
        if (t454_navLinks.length > 0) {
            t454_catchScroll(t454_navLinks)
        }
    }
}

function t454_catchScroll(t454_navLinks) {
    var t454_clickedSectionId = null, t454_sections = new Array(), t454_sectionIdTonavigationLink = [],
        t454_interval = 100, t454_lastCall, t454_timeoutId;
    t454_navLinks = $(t454_navLinks.get().reverse());
    t454_navLinks.each(function () {
        var t454_cursection = t454_getSectionByHref($(this));
        if (typeof t454_cursection.attr("id") != "undefined") {
            t454_sections.push(t454_cursection)
        }
        t454_sectionIdTonavigationLink[t454_cursection.attr("id")] = $(this)
    });
    t454_updateSectionsOffsets(t454_sections);
    $(window).bind('resize', t_throttle(function () {
        t454_updateSectionsOffsets(t454_sections)
    }, 200));
    $('.t454').bind('displayChanged', function () {
        t454_updateSectionsOffsets(t454_sections)
    });
    setInterval(function () {
        t454_updateSectionsOffsets(t454_sections)
    }, 5000);
    t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId);
    t454_navLinks.click(function () {
        if (!$(this).hasClass("tooltipstered")) {
            t454_navLinks.removeClass('t-active');
            t454_sectionIdTonavigationLink[t454_getSectionByHref($(this)).attr("id")].addClass('t-active');
            t454_clickedSectionId = t454_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t454_now = new Date().getTime();
        if (t454_lastCall && t454_now < (t454_lastCall + t454_interval)) {
            clearTimeout(t454_timeoutId);
            t454_timeoutId = setTimeout(function () {
                t454_lastCall = t454_now;
                t454_clickedSectionId = t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId)
            }, t454_interval - (t454_now - t454_lastCall))
        } else {
            t454_lastCall = t454_now;
            t454_clickedSectionId = t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId)
        }
    })
}

function t454_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t454_curSection = $(this);
        t454_curSection.attr("data-offset-top", t454_curSection.offset().top)
    })
}

function t454_getSectionByHref(curlink) {
    var t454_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t454_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t454_curLinkValue.substring(1) + "']")
    }
}

function t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId) {
    var t454_scrollPosition = $(window).scrollTop(), t454_valueToReturn = t454_clickedSectionId;
    if (t454_sections.length != 0 && t454_clickedSectionId == null && t454_sections[t454_sections.length - 1].attr("data-offset-top") > (t454_scrollPosition + 300)) {
        t454_navLinks.removeClass('t-active');
        return null
    }
    $(t454_sections).each(function (e) {
        var t454_curSection = $(this), t454_sectionTop = t454_curSection.attr("data-offset-top"),
            t454_id = t454_curSection.attr('id'), t454_navLink = t454_sectionIdTonavigationLink[t454_id];
        if (((t454_scrollPosition + 300) >= t454_sectionTop) || (t454_sections[0].attr("id") == t454_id && t454_scrollPosition >= $(document).height() - $(window).height())) {
            if (t454_clickedSectionId == null && !t454_navLink.hasClass('t-active')) {
                t454_navLinks.removeClass('t-active');
                t454_navLink.addClass('t-active');
                t454_valueToReturn = null
            } else {
                if (t454_clickedSectionId != null && t454_id == t454_clickedSectionId) {
                    t454_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t454_valueToReturn
}

function t454_setPath() {
}

function t454_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t454").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t454_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-80px");
                        el.css("visibility", "visible");
                        el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                        })
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden")
                }
            }
        })
    }
}

function t454_changebgopacitymenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            var bgopacityone = el.attr("data-bgopacity");
            var bgopacitytwo = el.attr("data-bgopacity-two");
            var menushadow = el.attr("data-menushadow");
            if (menushadow == '100') {
                var menushadowvalue = menushadow
            } else {
                var menushadowvalue = '0.' + menushadow
            }
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll);
                if (bgopacitytwo == '0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            } else {
                el.css("background-color", bgcolor);
                if (bgopacityone == '0.0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            }
        })
    }
}

function t454_createMobileMenu(recid) {
    var window_width = $(window).width(), el = $("#rec" + recid), menu = el.find(".t454"),
        burger = el.find(".t454__mobile");
    burger.click(function (e) {
        menu.fadeToggle(300);
        $(this).toggleClass("t454_opened")
    })
    $(window).bind('resize', t_throttle(function () {
        window_width = $(window).width();
        if (window_width > 980) {
            menu.fadeIn(0)
        }
    }, 200))
}

function t456_setListMagin(recid, imglogo) {
    if ($(window).width() > 980) {
        var t456__menu = $('#rec' + recid + ' .t456');
        var t456__leftpart = t456__menu.find('.t456__leftwrapper');
        var t456__listpart = t456__menu.find('.t456__list');
        if (imglogo) {
            t456__listpart.css("margin-right", t456__leftpart.width())
        } else {
            t456__listpart.css("margin-right", t456__leftpart.width() + 30)
        }
    }
}

function t456_highlight() {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t456__list_item a[href='" + url + "']").addClass("t-active");
    $(".t456__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t456__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t456__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t456__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t456__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t456_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t456_navLinks = $("#rec" + recid + " .t456__list_item a:not(.tooltipstered)[href*='#']");
        if (t456_navLinks.length > 0) {
            t456_catchScroll(t456_navLinks)
        }
    }
}

function t456_catchScroll(t456_navLinks) {
    var t456_clickedSectionId = null, t456_sections = new Array(), t456_sectionIdTonavigationLink = [],
        t456_interval = 100, t456_lastCall, t456_timeoutId;
    t456_navLinks = $(t456_navLinks.get().reverse());
    t456_navLinks.each(function () {
        var t456_cursection = t456_getSectionByHref($(this));
        if (typeof t456_cursection.attr("id") != "undefined") {
            t456_sections.push(t456_cursection)
        }
        t456_sectionIdTonavigationLink[t456_cursection.attr("id")] = $(this)
    });
    $(window).bind('resize', t_throttle(function () {
        t456_updateSectionsOffsets(t456_sections)
    }, 200));
    $('.t456').bind('displayChanged', function () {
        t456_updateSectionsOffsets(t456_sections)
    });
    setInterval(function () {
        t456_updateSectionsOffsets(t456_sections)
    }, 5000);
    setTimeout(function () {
        t456_updateSectionsOffsets(t456_sections);
        t456_highlightNavLinks(t456_navLinks, t456_sections, t456_sectionIdTonavigationLink, t456_clickedSectionId)
    }, 1000);
    t456_navLinks.click(function () {
        if (!$(this).hasClass("tooltipstered")) {
            t456_navLinks.removeClass('t-active');
            t456_sectionIdTonavigationLink[t456_getSectionByHref($(this)).attr("id")].addClass('t-active');
            t456_clickedSectionId = t456_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t456_now = new Date().getTime();
        if (t456_lastCall && t456_now < (t456_lastCall + t456_interval)) {
            clearTimeout(t456_timeoutId);
            t456_timeoutId = setTimeout(function () {
                t456_lastCall = t456_now;
                t456_clickedSectionId = t456_highlightNavLinks(t456_navLinks, t456_sections, t456_sectionIdTonavigationLink, t456_clickedSectionId)
            }, t456_interval - (t456_now - t456_lastCall))
        } else {
            t456_lastCall = t456_now;
            t456_clickedSectionId = t456_highlightNavLinks(t456_navLinks, t456_sections, t456_sectionIdTonavigationLink, t456_clickedSectionId)
        }
    })
}

function t456_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t456_curSection = $(this);
        t456_curSection.attr("data-offset-top", t456_curSection.offset().top)
    })
}

function t456_getSectionByHref(curlink) {
    var t456_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t456_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t456_curLinkValue.substring(1) + "']")
    }
}

function t456_highlightNavLinks(t456_navLinks, t456_sections, t456_sectionIdTonavigationLink, t456_clickedSectionId) {
    var t456_scrollPosition = $(window).scrollTop(), t456_valueToReturn = t456_clickedSectionId;
    if (t456_sections.length != 0 && t456_clickedSectionId == null && t456_sections[t456_sections.length - 1].attr("data-offset-top") > (t456_scrollPosition + 300)) {
        t456_navLinks.removeClass('t-active');
        return null
    }
    $(t456_sections).each(function (e) {
        var t456_curSection = $(this), t456_sectionTop = t456_curSection.attr("data-offset-top"),
            t456_id = t456_curSection.attr('id'), t456_navLink = t456_sectionIdTonavigationLink[t456_id];
        if (((t456_scrollPosition + 300) >= t456_sectionTop) || (t456_sections[0].attr("id") == t456_id && t456_scrollPosition >= $(document).height() - $(window).height())) {
            if (t456_clickedSectionId == null && !t456_navLink.hasClass('t-active')) {
                t456_navLinks.removeClass('t-active');
                t456_navLink.addClass('t-active');
                t456_valueToReturn = null
            } else {
                if (t456_clickedSectionId != null && t456_id == t456_clickedSectionId) {
                    t456_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t456_valueToReturn
}

function t456_setPath() {
}

function t456_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t456").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t456").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t456_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t456").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-50px");
                        el.css("visibility", "visible");
                        el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                        })
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden")
                }
            }
        })
    }
}

function t456_changebgopacitymenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t456").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            var bgopacityone = el.attr("data-bgopacity");
            var bgopacitytwo = el.attr("data-bgopacity-two");
            var menushadow = el.attr("data-menushadow");
            if (menushadow == '100') {
                var menushadowvalue = menushadow
            } else {
                var menushadowvalue = '0.' + menushadow
            }
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll);
                if (bgopacitytwo == '0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            } else {
                el.css("background-color", bgcolor);
                if (bgopacityone == '0.0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            }
        })
    }
}

function t456_createMobileMenu(recid) {
    var window_width = $(window).width(), el = $("#rec" + recid), menu = el.find(".t456"),
        burger = el.find(".t456__mobile");
    burger.click(function (e) {
        menu.fadeToggle(300);
        $(this).toggleClass("t456_opened")
    });
    $(window).bind('resize', t_throttle(function () {
        window_width = $(window).width();
        if (window_width > 980) {
            menu.fadeIn(0)
        }
    }, 200))
}

function t461_setLogoPadding(recid) {
    if ($(window).width() > 980) {
        var t461__menu = $('#rec' + recid + ' .t461');
        var t461__logo = t461__menu.find('.t461__logowrapper');
        var t461__leftpart = t461__menu.find('.t461__leftwrapper');
        var t461__rightpart = t461__menu.find('.t461__rightwrapper');
        t461__leftpart.css("padding-right", t461__logo.width() / 2 + 50);
        t461__rightpart.css("padding-left", t461__logo.width() / 2 + 50)
    }
}

function t461_highlight() {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t461__list_item a[href='" + url + "']").addClass("t-active");
    $(".t461__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t461__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t461__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t461__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t461__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t461_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t461_navLinks = $("#rec" + recid + " .t461__desktoplist .t461__list_item a:not(.tooltipstered)[href*='#']");
        if (t461_navLinks.length > 0) {
            t461_catchScroll(t461_navLinks)
        }
    }
}

function t461_catchScroll(t461_navLinks) {
    var t461_clickedSectionId = null, t461_sections = new Array(), t461_sectionIdTonavigationLink = [],
        t461_interval = 100, t461_lastCall, t461_timeoutId;
    t461_navLinks = $(t461_navLinks.get().reverse());
    t461_navLinks.each(function () {
        var t461_cursection = t461_getSectionByHref($(this));
        if (typeof t461_cursection.attr("id") != "undefined") {
            t461_sections.push(t461_cursection)
        }
        t461_sectionIdTonavigationLink[t461_cursection.attr("id")] = $(this)
    });
    t461_updateSectionsOffsets(t461_sections);
    t461_sections.sort(function (a, b) {
        return b.attr("data-offset-top") - a.attr("data-offset-top")
    });
    $(window).bind('resize', t_throttle(function () {
        t461_updateSectionsOffsets(t461_sections)
    }, 200));
    $('.t461').bind('displayChanged', function () {
        t461_updateSectionsOffsets(t461_sections)
    });
    setInterval(function () {
        t461_updateSectionsOffsets(t461_sections)
    }, 5000);
    t461_highlightNavLinks(t461_navLinks, t461_sections, t461_sectionIdTonavigationLink, t461_clickedSectionId);
    t461_navLinks.click(function () {
        var t461_clickedSection = t461_getSectionByHref($(this));
        if (!$(this).hasClass("tooltipstered") && typeof t461_clickedSection.attr("id") != "undefined") {
            t461_navLinks.removeClass('t-active');
            $(this).addClass('t-active');
            t461_clickedSectionId = t461_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t461_now = new Date().getTime();
        if (t461_lastCall && t461_now < (t461_lastCall + t461_interval)) {
            clearTimeout(t461_timeoutId);
            t461_timeoutId = setTimeout(function () {
                t461_lastCall = t461_now;
                t461_clickedSectionId = t461_highlightNavLinks(t461_navLinks, t461_sections, t461_sectionIdTonavigationLink, t461_clickedSectionId)
            }, t461_interval - (t461_now - t461_lastCall))
        } else {
            t461_lastCall = t461_now;
            t461_clickedSectionId = t461_highlightNavLinks(t461_navLinks, t461_sections, t461_sectionIdTonavigationLink, t461_clickedSectionId)
        }
    })
}

function t461_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t461_curSection = $(this);
        t461_curSection.attr("data-offset-top", t461_curSection.offset().top)
    })
}

function t461_getSectionByHref(curlink) {
    var t461_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (t461_curLinkValue[0] == '/') {
        t461_curLinkValue = t461_curLinkValue.substring(1)
    }
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t461_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t461_curLinkValue.substring(1) + "']")
    }
}

function t461_highlightNavLinks(t461_navLinks, t461_sections, t461_sectionIdTonavigationLink, t461_clickedSectionId) {
    var t461_scrollPosition = $(window).scrollTop(), t461_valueToReturn = t461_clickedSectionId;
    if (t461_sections.length != 0 && t461_clickedSectionId == null && t461_sections[t461_sections.length - 1].attr("data-offset-top") > (t461_scrollPosition + 300)) {
        t461_navLinks.removeClass('t-active');
        return null
    }
    $(t461_sections).each(function (e) {
        var t461_curSection = $(this), t461_sectionTop = t461_curSection.attr("data-offset-top"),
            t461_id = t461_curSection.attr('id'), t461_navLink = t461_sectionIdTonavigationLink[t461_id];
        if (((t461_scrollPosition + 300) >= t461_sectionTop) || (t461_sections[0].attr("id") == t461_id && t461_scrollPosition >= $(document).height() - $(window).height())) {
            if (t461_clickedSectionId == null && !t461_navLink.hasClass('t-active')) {
                t461_navLinks.removeClass('t-active');
                t461_navLink.addClass('t-active');
                t461_valueToReturn = null
            } else {
                if (t461_clickedSectionId != null && t461_id == t461_clickedSectionId) {
                    t461_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t461_valueToReturn
}

function t461_setPath() {
}

function t461_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t461").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t461").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t461_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t461").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-50px");
                        el.css("visibility", "visible");
                        el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                        })
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden")
                }
            }
        })
    }
}

function t461_changebgopacitymenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t461").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            var bgopacityone = el.attr("data-bgopacity");
            var bgopacitytwo = el.attr("data-bgopacity-two");
            var menushadow = el.attr("data-menushadow");
            if (menushadow == '100') {
                var menushadowvalue = menushadow
            } else {
                var menushadowvalue = '0.' + menushadow
            }
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll);
                if (bgopacitytwo == '0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            } else {
                el.css("background-color", bgcolor);
                if (bgopacityone == '0.0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            }
        })
    }
}

function t461_createMobileMenu(recid) {
    var window_width = $(window).width(), el = $("#rec" + recid), menu = el.find(".t461"),
        burger = el.find(".t461__mobile");
    burger.click(function (e) {
        menu.fadeToggle(300);
        $(this).toggleClass("t461_opened")
    })
    $(window).bind('resize', t_throttle(function () {
        window_width = $(window).width();
        if (window_width > 980) {
            menu.fadeIn(0)
        }
    }, 200))
}

function t480_setHeight(recid) {
    var el = $('#rec' + recid);
    var sizer = el.find('.t480__sizer');
    var height = sizer.height();
    var width = sizer.width();
    var ratio = width / height;
    var imgwrapper = el.find(".t480__blockimg, .t480__textwrapper");
    var imgwidth = imgwrapper.width();
    if (height != $(window).height()) {
        imgwrapper.css({'height': ((imgwidth / ratio) + 'px')})
    }
}

function t533_equalHeight(recid) {
    var el = $('#rec' + recid);
    el.find('.t533').css('visibility', 'visible');
    el.find('.t533__textwrapper').css('height', 'auto');
    $('#rec' + recid + ' .t533__row').each(function () {
        var highestBox = 0;
        $('.t533__textwrapper', this).each(function () {
            if ($(this).height() > highestBox) highestBox = $(this).height()
        });
        if ($(window).width() >= 960 && $(this).is(':visible')) {
            $('.t533__textwrapper', this).css('height', highestBox)
        } else {
            $('.t533__textwrapper', this).css('height', "auto")
        }
    })
};

function t552_init(recid, ratio) {
    var t552__el = $("#rec" + recid), t552__image = t552__el.find(".t552__blockimg:first");
    t552__setHeight(recid, t552__image, ratio);
    var t552__doResize;
    $(window).resize(function () {
        clearTimeout(t552__doResize);
        t552__doResize = setTimeout(function () {
            t552__setHeight(recid, t552__image, ratio)
        }, 200)
    })
}

function t552__setHeight(recid, image, ratio) {
    $("#rec" + recid + " .t552__blockimg").css("height", Math.round(image.innerWidth() * ratio))
}

function t585_init(recid) {
    var el = $('#rec' + recid), toggler = el.find(".t585__header");
    toggler.click(function () {
        $(this).toggleClass("t585__opened");
        $(this).next().slideToggle();
        if (window.lazy == 'y') {
            t_lazyload_update()
        }
    })
}

function t670_init(recid) {
    t670_imageHeight(recid);
    t670_show(recid);
    t670_hide(recid)
}

function t670_show(recid) {
    var el = $("#rec" + recid), play = el.find('.t670__play');
    play.click(function () {
        if ($(this).attr('data-slider-video-type') == 'youtube') {
            var url = $(this).attr('data-slider-video-url');
            $(this).next().html("<iframe class=\"t670__iframe\" width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/" + url + "?autoplay=1\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>")
        }
        if ($(this).attr('data-slider-video-type') == 'vimeo') {
            var url = $(this).attr('data-slider-video-url');
            $(this).next().html("<iframe class=\"t670__iframe\" width=\"100%\" height=\"100%\" src=\"https://player.vimeo.com/video/" + url + "?autoplay=1\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>")
        }
        $(this).next().css('z-index', '3')
    })
}

function t670_hide(recid) {
    var el = $("#rec" + recid), body = el.find('.t670__frame');
    el.on('updateSlider', function () {
        body.html('').css('z-index', '')
    })
}

function t670_imageHeight(recid) {
    var el = $("#rec" + recid);
    var image = el.find(".t670__separator");
    image.each(function () {
        var width = $(this).attr("data-slider-image-width");
        var height = $(this).attr("data-slider-image-height");
        var ratio = height / width;
        var padding = ratio * 100;
        $(this).css("padding-bottom", padding + "%")
    })
}

function t678_onSuccess(t678_form) {
    var t678_inputsWrapper = t678_form.find('.t-form__inputsbox');
    var t678_inputsHeight = t678_inputsWrapper.height();
    var t678_inputsOffset = t678_inputsWrapper.offset().top;
    var t678_inputsBottom = t678_inputsHeight + t678_inputsOffset;
    var t678_targetOffset = t678_form.find('.t-form__successbox').offset().top;
    if ($(window).width() > 960) {
        var t678_target = t678_targetOffset - 200
    } else {
        var t678_target = t678_targetOffset - 100
    }
    if (t678_targetOffset > $(window).scrollTop() || ($(document).height() - t678_inputsBottom) < ($(window).height() - 100)) {
        t678_inputsWrapper.addClass('t678__inputsbox_hidden');
        setTimeout(function () {
            if ($(window).height() > $('.t-body').height()) {
                $('.t-tildalabel').animate({opacity: 0}, 50)
            }
        }, 300)
    } else {
        $('html, body').animate({scrollTop: t678_target}, 400);
        setTimeout(function () {
            t678_inputsWrapper.addClass('t678__inputsbox_hidden')
        }, 400)
    }
    var successurl = t678_form.data('success-url');
    if (successurl && successurl.length > 0) {
        setTimeout(function () {
            window.location.href = successurl
        }, 500)
    }
}

function t690_onSuccess(t690_form) {
    var t690_inputsWrapper = t690_form.find('.t-form__inputsbox');
    var t690_inputsHeight = t690_inputsWrapper.height();
    var t690_inputsOffset = t690_inputsWrapper.offset().top;
    var t690_inputsBottom = t690_inputsHeight + t690_inputsOffset;
    var t690_targetOffset = t690_form.find('.t-form__successbox').offset().top;
    if ($(window).width() > 960) {
        var t690_target = t690_targetOffset - 200
    } else {
        var t690_target = t690_targetOffset - 100
    }
    if (t690_targetOffset > $(window).scrollTop() || ($(document).height() - t690_inputsBottom) < ($(window).height() - 100)) {
        t690_inputsWrapper.addClass('t690__inputsbox_hidden');
        setTimeout(function () {
            if ($(window).height() > $('.t-body').height()) {
                $('.t-tildalabel').animate({opacity: 0}, 50)
            }
        }, 300)
    } else {
        $('html, body').animate({scrollTop: t690_target}, 400);
        setTimeout(function () {
            t690_inputsWrapper.addClass('t690__inputsbox_hidden')
        }, 400)
    }
    var successurl = t690_form.data('success-url');
    if (successurl && successurl.length > 0) {
        setTimeout(function () {
            window.location.href = successurl
        }, 500)
    }
}

function t691_unifyHeights(recid) {
    if ($(window).width() >= 960) {
        $('#rec' + recid + ' .t691 .t-container .t691__row').each(function () {
            var t691__highestBox = 0;
            var t691__currow = $(this);
            $('.t691__col', this).each(function () {
                var t691__curcol = $(this);
                var t691__curcolinfo = t691__curcol.find('.t691__sectioninfowrapper');
                var t691__curcolpers = t691__curcol.find('.t691__personwrapper');
                var t691__curcolinnerheight = t691__curcolinfo.outerHeight() + t691__curcolpers.outerHeight();
                if (t691__curcolinnerheight > t691__highestBox) {
                    t691__highestBox = t691__curcolinnerheight
                }
            });
            $('.t691__col', this).css('height', t691__highestBox)
        })
    }
};

function t696_onSuccess(t696_form) {
    var t696_inputsWrapper = t696_form.find('.t-form__inputsbox');
    var t696_inputsHeight = t696_inputsWrapper.height();
    var t696_inputsOffset = t696_inputsWrapper.offset().top;
    var t696_inputsBottom = t696_inputsHeight + t696_inputsOffset;
    var t696_targetOffset = t696_form.find('.t-form__successbox').offset().top;
    if ($(window).width() > 960) {
        var t696_target = t696_targetOffset - 200
    } else {
        var t696_target = t696_targetOffset - 100
    }
    if (t696_targetOffset > $(window).scrollTop() || ($(document).height() - t696_inputsBottom) < ($(window).height() - 100)) {
        t696_inputsWrapper.addClass('t696__inputsbox_hidden');
        setTimeout(function () {
            if ($(window).height() > $('.t-body').height()) {
                $('.t-tildalabel').animate({opacity: 0}, 50)
            }
        }, 300)
    } else {
        $('html, body').animate({scrollTop: t696_target}, 400);
        setTimeout(function () {
            t696_inputsWrapper.addClass('t696__inputsbox_hidden')
        }, 400)
    }
    var successurl = t696_form.data('success-url');
    if (successurl && successurl.length > 0) {
        setTimeout(function () {
            window.location.href = successurl
        }, 500)
    }
}

function t762_init(recid) {
    t_sldsInit(recid);
    setTimeout(function () {
        t_prod__init(recid)
    }, 500);
    $('#rec' + recid).find('.t762').bind('displayChanged', function () {
        t_slds_updateSlider(recid)
    })
}

function t778__init(recid) {
    t778_unifyHeights(recid);
    $(window).load(function () {
        t778_unifyHeights(recid)
    });
    $(window).bind('resize', t_throttle(function () {
        t778_unifyHeights(recid)
    }, 200));
    $(".t778").bind("displayChanged", function () {
        t778_unifyHeights(recid)
    });
    setTimeout(function () {
        t_prod__init(recid);
        t778_initPopup(recid);
        t778__updateLazyLoad(recid)
    }, 500)
}

function t778__updateLazyLoad(recid) {
    var scrollContainer = $("#rec" + recid + " .t778__container_mobile-flex");
    var curMode = $(".t-records").attr("data-tilda-mode");
    if (scrollContainer.length && curMode != "edit" && curMode != "preview") {
        scrollContainer.bind('scroll', t_throttle(function () {
            t_lazyload_update()
        }, 500))
    }
}

function t778_unifyHeights(recid) {
    var t778_el = $('#rec' + recid), t778_blocksPerRow = t778_el.find(".t778__container").attr("data-blocks-per-row"),
        t778_cols = t778_el.find(".t778__content"), t778_mobScroll = t778_el.find(".t778__scroll-icon-wrapper").length;
    if ($(window).width() <= 480 && t778_mobScroll == 0) {
        t778_cols.css("height", "auto");
        return
    }
    var t778_perRow = +t778_blocksPerRow;
    if ($(window).width() <= 960 && t778_mobScroll > 0) {
        var t778_perRow = t778_cols.length
    }
    else {
        if ($(window).width() <= 960) {
            var t778_perRow = 2
        }
    }
    for (var i = 0; i < t778_cols.length; i += t778_perRow) {
        var t778_maxHeight = 0, t778_row = t778_cols.slice(i, i + t778_perRow);
        t778_row.each(function () {
            var t778_curText = $(this).find(".t778__textwrapper"),
                t778_curBtns = $(this).find(".t778__btn-wrapper_absolute"),
                t778_itemHeight = t778_curText.outerHeight() + t778_curBtns.outerHeight();
            if (t778_itemHeight > t778_maxHeight) {
                t778_maxHeight = t778_itemHeight
            }
        });
        t778_row.css("height", t778_maxHeight)
    }
}

function t778_initPopup(recid) {
    var rec = $('#rec' + recid);
    rec.find('[href^="#prodpopup"]').one("click", function (e) {
        e.preventDefault();
        var el_popup = rec.find('.t-popup');
        var el_prod = $(this).closest('.js-product');
        var lid_prod = el_prod.attr('data-product-lid');
        t_sldsInit(recid + ' #t778__product-' + lid_prod + '')
    });
    rec.find('[href^="#prodpopup"]').click(function (e) {
        e.preventDefault();
        t778_showPopup(recid);
        var el_popup = rec.find('.t-popup');
        var el_prod = $(this).closest('.js-product');
        var lid_prod = el_prod.attr('data-product-lid');
        el_popup.find('.js-product').css('display', 'none');
        var el_fullprod = el_popup.find('.js-product[data-product-lid="' + lid_prod + '"]')
        el_fullprod.css('display', 'block');
        var analitics = el_popup.attr('data-track-popup');
        if (analitics > '') {
            var virtTitle = el_fullprod.find('.js-product-name').text();
            if (!virtTitle) {
                virtTitle = 'prod' + lid_prod
            }
            Tilda.sendEventToStatistics(analitics, virtTitle)
        }
        var curUrl = window.location.href;
        if (curUrl.indexOf('#!/tproduct/') < 0 && curUrl.indexOf('%23!/tproduct/') < 0) {
            if (typeof history.replaceState != 'undefined') {
                window.history.replaceState('', '', window.location.href + '#!/tproduct/' + recid + '-' + lid_prod)
            }
        }
        t778_updateSlider(recid + ' #t778__product-' + lid_prod + '');
        if (window.lazy == 'y') {
            t_lazyload_update()
        }
    });
    if ($('#record' + recid).length == 0) {
        t778_checkUrl(recid)
    }
    t778_copyTypography(recid)
}

function t778_checkUrl(recid) {
    var curUrl = window.location.href;
    var tprodIndex = curUrl.indexOf('#!/tproduct/');
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent) && tprodIndex < 0) {
        tprodIndex = curUrl.indexOf('%23!/tproduct/')
    }
    if (tprodIndex >= 0) {
        var curUrl = curUrl.substring(tprodIndex, curUrl.length);
        var curProdLid = curUrl.substring(curUrl.indexOf('-') + 1, curUrl.length);
        var rec = $('#rec' + recid);
        if (curUrl.indexOf(recid) >= 0 && rec.find('[data-product-lid=' + curProdLid + ']').length) {
            rec.find('[data-product-lid=' + curProdLid + '] [href^="#prodpopup"]').triggerHandler('click')
        }
    }
}

function t778_updateSlider(recid) {
    var el = $('#rec' + recid);
    t_slds_SliderWidth(recid);
    var sliderWrapper = el.find('.t-slds__items-wrapper');
    var sliderWidth = el.find('.t-slds__container').width();
    var pos = parseFloat(sliderWrapper.attr('data-slider-pos'));
    sliderWrapper.css({transform: 'translate3d(-' + (sliderWidth * pos) + 'px, 0, 0)'});
    t_slds_UpdateSliderHeight(recid);
    t_slds_UpdateSliderArrowsHeight(recid)
}

function t778_showPopup(recid) {
    var el = $('#rec' + recid);
    var popup = el.find('.t-popup');
    popup.css('display', 'block');
    setTimeout(function () {
        popup.find('.t-popup__container').addClass('t-popup__container-animated');
        popup.addClass('t-popup_show');
        if (window.lazy == 'y') {
            t_lazyload_update()
        }
    }, 50);
    $('body').addClass('t-body_popupshowed');
    el.find('.t-popup').click(function (e) {
        if (e.target == this) {
            t778_closePopup()
        }
    });
    el.find('.t-popup__close, .t778__close-text').click(function (e) {
        t778_closePopup()
    });
    $(document).keydown(function (e) {
        if (e.keyCode == 27) {
            t778_closePopup()
        }
    })
}

function t778_closePopup() {
    $('body').removeClass('t-body_popupshowed');
    $('.t-popup').removeClass('t-popup_show');
    var curUrl = window.location.href;
    var indexToRemove = curUrl.indexOf('#!/tproduct/');
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent) && indexToRemove < 0) {
        indexToRemove = curUrl.indexOf('%23!/tproduct/')
    }
    curUrl = curUrl.substring(0, indexToRemove);
    setTimeout(function () {
        $(".t-popup").scrollTop(0);
        $('.t-popup').not('.t-popup_show').css('display', 'none');
        if (typeof history.replaceState != 'undefined') {
            window.history.replaceState('', '', curUrl)
        }
    }, 300)
}

function t778_removeSizeStyles(styleStr) {
    if (typeof styleStr != "undefined" && (styleStr.indexOf('font-size') >= 0 || styleStr.indexOf('padding-top') >= 0 || styleStr.indexOf('padding-bottom') >= 0)) {
        var styleStrSplitted = styleStr.split(';');
        styleStr = "";
        for (var i = 0; i < styleStrSplitted.length; i++) {
            if (styleStrSplitted[i].indexOf('font-size') >= 0 || styleStrSplitted[i].indexOf('padding-top') >= 0 || styleStrSplitted[i].indexOf('padding-bottom') >= 0) {
                styleStrSplitted.splice(i, 1);
                i--;
                continue
            }
            if (styleStrSplitted[i] == "") {
                continue
            }
            styleStr += styleStrSplitted[i] + ";"
        }
    }
    return styleStr
}

function t778_copyTypography(recid) {
    var rec = $('#rec' + recid);
    var titleStyle = rec.find('.t778__title').attr('style');
    var descrStyle = rec.find('.t778__descr').attr('style');
    rec.find('.t-popup .t778__title').attr("style", t778_removeSizeStyles(titleStyle));
    rec.find('.t-popup .t778__descr, .t-popup .t778__text').attr("style", t778_removeSizeStyles(descrStyle))
}

function t825_initPopup(recid) {
    $('#rec' + recid).attr('data-animationappear', 'off');
    $('#rec' + recid).css('opacity', '1');
    var el = $('#rec' + recid).find('.t825__popup'), analitics = el.attr('data-track-popup'),
        hook = "TildaSendMessageWidget" + recid, obj = $('#rec' + recid + ' .t825__btn');
    obj.click(function (e) {
        if (obj.hasClass("t825__btn_active")) {
            t825_closePopup();
            return
        }
        obj.addClass("t825__btn_active");
        $('#rec' + recid + ' .t825').addClass("t825_active");
        t825_showPopup(recid);
        e.preventDefault();
        if (analitics > '') {
            Tilda.sendEventToStatistics(analitics, hook)
        }
    });
    if (window.lazy == 'y') {
        t_lazyload_update()
    }
    t825_checkPhoneNumber(recid)
}

function t825_showPopup(recid) {
    var el = $('#rec' + recid), popup = el.find('.t825__popup');
    $('.t825__btn_wrapper').removeClass('t825__btn_animate');
    $('.t825__btn-text').css('display', 'none');
    if ($(window).width() < 960) {
        $('body').addClass('t825__body_popupshowed')
    }
    popup.css('display', 'block');
    setTimeout(function () {
        popup.addClass('t825__popup_show')
    }, 50);
    el.find(".t825__mobile-icon-close").click(function (e) {
        t825_closePopup()
    });
    $(document).keydown(function (e) {
        if (e.keyCode == 27) {
            t825_closePopup()
        }
    });
    if (window.lazy == 'y') {
        t_lazyload_update()
    }
}

function t825_closePopup() {
    if ($(window).width() < 960) {
        $('body').removeClass('t825__body_popupshowed')
    }
    $('.t825').removeClass("t825_active");
    $('.t825__btn').removeClass('t825__btn_active');
    $('.t825__popup').removeClass('t825__popup_show');
    setTimeout(function () {
        $('.t825__popup').not('.t825__popup_show').css('display', 'none')
    }, 300)
}

function t825_checkPhoneNumber(recid) {
    var el = $('#rec' + recid);
    var whatsapp = el.find('.t825__whatsapp').attr('href');
    var telegram = el.find('.t825__telegram').attr('href');
    var vk = el.find('.t825__vk').attr('href');
    var skype = el.find('.t825__skype').attr('href');
    var mail = el.find('.t825__mail').attr('href');
    var viber = el.find('.t825__viber').attr('href');
    var fb = el.find('.t825__fb').attr('href');
    var phone = el.find('.t825__phone').attr('href');
    if (typeof whatsapp != 'undefined') {
        el.find('.t825__whatsapp').attr('href', 'https://api.whatsapp.com/send?phone=' + whatsapp.replace(/[+?^${}()|[\]\\\s]/g, ''))
    }
    el.find('.t825__telegram').attr('href', 'https://t.me/' + telegram);
    el.find('.t825__vk').attr('href', 'https://vk.me/' + vk);
    if (typeof skype != 'undefined') {
        el.find('.t825__skype').attr('href', 'skype:' + skype.replace(/[+?^${}()|[\]\\\s]/g, ''))
    }
    if (typeof viber != 'undefined') {
        el.find('.t825__viber').attr('href', 'viber://chat?number=%2B' + viber.replace(/[+?^${}()|[\]\\\s]/g, ''))
    }
    el.find('.t825__mail').attr('href', 'mailto:' + mail);
    el.find('.t825__fb').attr('href', 'https://www.messenger.com/t/' + fb);
    if (typeof phone != 'undefined') {
        el.find('.t825__phone').attr('href', 'tel:+' + phone.replace(/[+?^${}()|[\]\\\s]/g, ''))
    }
}

function t825_sendPopupEventToStatistics(popupname) {
    var virtPage = '/tilda/popup/';
    var virtTitle = 'Popup: ';
    if (popupname.substring(0, 7) == '#popup:') {
        popupname = popupname.substring(7)
    }
    virtPage += popupname;
    virtTitle += popupname;
    if (window.Tilda && typeof Tilda.sendEventToStatistics == 'function') {
        Tilda.sendEventToStatistics(virtPage, virtTitle, '', 0)
    } else {
        if (ga) {
            if (window.mainTracker != 'tilda') {
                ga('send', {'hitType': 'pageview', 'page': virtPage, 'title': virtTitle})
            }
        }
        if (window.mainMetrika > '' && window[window.mainMetrika]) {
            window[window.mainMetrika].hit(virtPage, {title: virtTitle, referer: window.location.href})
        }
    }
}