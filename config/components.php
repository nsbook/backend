<?php

return [
    'mongodb' => [
        'class' => '\yii\mongodb\Connection',
        'dsn' => 'mongodb://127.0.0.1/nsbook',
        'options' => [
            'connectTimeoutMS' => 1000
        ]
    ],
    'assetManager' => [
        'bundles' => [
            'yii\web\JqueryAsset' => [
                'sourcePath' => null,   // do not publish the bundle
                'js' => [
                    '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
                ]
            ],
        ],
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'useFileTransport' => true,
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
            [
                'class' => 'app\components\MongoDbTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
];