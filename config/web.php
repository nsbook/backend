<?php

$params = require __DIR__ . '/params.php';
$components = require __DIR__ . '/components.php';

$components = array_merge($components, [
    'errorHandler' => [
        'errorAction' => 'site/error'
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
        ],
    ],
    'user' => [
        'identityClass' => 'app\models\User',
        'enableAutoLogin' => true,
        'loginUrl' => ['/auth/login']
    ],
    'request' => [
        'cookieValidationKey' => '77fchfuuaIjKEbajhJNGVOo9Ixt07GGj',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'instagram' => [
        'class' => 'app\components\Instagram'
]
]);

$config = [
    'id' => 'basic',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'manage' => [
            'class' => 'app\modules\manage\Module',
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploaded',
            'uploadUrl' => '@web/uploaded',
            'imageAllowExtensions'=>['jpg','png'],
            'widgetClientOptions' => [
                'imageManagerJson' => ['/redactor/upload/image-json'],
                'imageUpload' => ['/redactor/upload/image'],
                'fileUpload' => ['/redactor/upload/file'],
                'lang' => 'ru',
                'plugins' => ['clips', 'fontcolor','imagemanager']
            ]
        ],
    ],
    'components' => $components,
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
  //  $config['bootstrap'][] = 'debug';
 //   $config['modules']['debug'] = [
   //     'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
 //       //'allowedIPs' => ['127.0.0.1', '::1'],
  //  ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'mongoDbModel' =>[
                'class' => 'yii\mongodb\gii\model\Generator'
            ]
        ]
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
