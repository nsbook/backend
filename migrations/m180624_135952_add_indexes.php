<?php

class m180624_135952_add_indexes extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createIndex('insta_account', ['username'], ['unique' => true]);
        $this->createIndex('insta_data', ['created_at', 'insta_id']);
        $this->createIndex('insta_data', ['insta_id', 'media_id'], ['unique' => true]);
    }

    public function down()
    {
        echo "m180624_135952_add_indexes cannot be reverted.\n";

        return false;
    }
}
