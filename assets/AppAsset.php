<?php

namespace app\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/grid-3.0.min.css',
        'css/blocks-2.12.css',
        'css/animation-1.0.min.css',
        'css/slds-1.4.min.css',
        'css/zoom-2.0.min.css',
        'css/popup-1.1.min.css',
        'css/tooltipster.min.css',
        'css/date-picker-1.5.css',
        'css/site.css',
    ];
    public $js = [
        'js/scripts-2.8.min.js',
        'js/blocks-2.7.js',
        'js/lazyload-1.3.min.js',
        'js/animation-1.0.min.js',
        'js/slds-1.4.min.js',
        'js/hammer.min.js',
        'js/zoom-2.0.min.js',
        'js/animation-ext-1.0.min.js',
        'js/tooltipster.min.js',
        'js/date-picker-1.2.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
