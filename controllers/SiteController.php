<?php

namespace app\controllers;

use app\forms\FeedbackForm;
use app\models\Faq;
use app\models\Gallery;
use app\models\InstaAccount;
use app\models\InstaData;
use app\models\Review;
use app\models\Video;
use InstagramScraper\Exception\InstagramNotFoundException;
use Yii;
use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\mongodb\Query;
use yii\web\Controller;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => 'testme',
                'foreColor' => 0x000000
            ],
        ];
    }

    public function actionIndex()
    {
        $galleries = Gallery::find()->orderBy(['priority' => 1])->all();
        $reviews = Review::findAll(['is_visible' => true]);
        $video = Video::find()->one();
        $faqs = Faq::findAll(['is_visible' => true]);
        return $this->render('index', [
            'galleries' => $galleries,
            'reviews' => $reviews,
            'video' => $video,
            'faqs' => $faqs
        ]);
    }

    public function actionGetMedia()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dateFrom = Yii::$app->request->getQueryParam('dateFrom');
        $dateTo = Yii::$app->request->getQueryParam('dateTo');
        $userId = Yii::$app->request->getQueryParam('userId');
        $from = strtotime($dateFrom. ' 00:00:00');
        $to = strtotime($dateTo. ' 23:59:59');

        $query = new Query();
        $query->from('insta_data');
        $query->where(['insta_id' => (int) $userId]);
        $query->andWhere(['>=', 'created_at', $from]);
        $query->andWhere(['<=', 'created_at', $to]);

        $data = InstaData::find()->where([
            'insta_id' => (int) $userId,
            'created_at' => [
                '$gte' => $from,
                '$lte' => $to
            ]
        ])->orderBy(['created_at' => SORT_DESC])->limit(16)->offset(0)->all();

        return ['success' => true, 'data' => $data];
    }

    public function actionContact()
    {
        $model = new FeedbackForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Ваше обращение принято!');
        }
        return $this->render('_feedback', ['model' => $model]);
    }

    public function actionInsta()
    {
        try {
            $username = Yii::$app->request->getQueryParam('login');
            $instaAccount = InstaAccount::find()->where(['username' => $username])->one();
            $instagram = new \InstagramScraper\Instagram();
            $account = $instagram->getAccount($username);
        } catch (InstagramNotFoundException $e) {
            Yii::getLogger()->log($e->getMessage() . " - {$username}", Logger::LEVEL_WARNING, 'FindAccount');
            $account = null;
        }

        if (!is_null($account) && is_null($instaAccount)) {
            $instaAccount = new InstaAccount();
            $instaAccount->insta_id = $account->getId();
            $instaAccount->username = $account->getUsername();
            $instaAccount->full_name = $account->getFullName();
            $instaAccount->profile_pic_url = $account->getProfilePicUrl();
            $instaAccount->biography = $account->getBiography();
            $instaAccount->follows_count = $account->getFollowsCount();
            $instaAccount->followed_by_count = $account->getFollowedByCount();
            $instaAccount->media_count = $account->getMediaCount();
            $instaAccount->is_active = false;
            $instaAccount->save();
        }

        return $this->render('insta', ['user' => $instaAccount]);
    }


    public function actionPhoto()
    {
        return $this->render('photo');
    }
}
