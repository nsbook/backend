<?php /* @var $this \yii\web\View */ ?>
<?php /* @var $content string */ ?>
<?php use yii\helpers\Html; ?>
<?php use app\assets\AppAsset; ?>
<?php AppAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=1280"/>
    <script type="text/javascript">window.noAdaptive = true;</script>
    <meta property="og:title" content="<?= Html::encode($this->title); ?>"/>
    <meta property="og:description" content="<?= Html::encode($this->title); ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="<?= Yii::getAlias("@web"); ?>/logo-header.png"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="description" content="<?= Html::encode($this->title); ?>"/>
    <link rel="icon" type="image/png" href="<?= Yii::getAlias("@web"); ?>/logo-header.png"/>
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body class="t-body" style="margin:0;">
<?= $this->render("_header"); ?>
<?php $this->beginBody(); ?>
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="359550"
     data-tilda-page-id="2433940" data-tilda-page-alias="nsbook2" data-tilda-formskey="77cb59c4071f895e54182c3105a11260"
     data-blocks-animationoff="yes">
    <?= $content; ?>
    <?= $this->render("_footer"); ?>
</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
