<?php /** @var \yii\web\View $this */ ?>
<?php /** @var string $content */ ?>
<?php \app\assets\ManageAsset::register($this); ?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta charset="<?= Yii::$app->charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= yii\helpers\Html::csrfMetaTags() ?>
    <title><?= yii\helpers\Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="wrap <?php if (Yii::$app->user->isGuest): ?>wrap-login<?php endif; ?>">
    <?php if (!Yii::$app->user->isGuest): ?>
        <?php
        yii\bootstrap\NavBar::begin([
            "brandLabel" => "Управление",
            "brandUrl" => ["/manage"],
            "options" => [
                "class" => "navbar-inverse navbar-fixed-top",
            ],
        ]);
        echo yii\bootstrap\Nav::widget([
            "options" => ["class" => "navbar-nav navbar-right"],
            "activateParents" => true,
            "items" => [
                ["label" => "Логи", "url" => ["/manage/log/index"]],
                ["label" => "Обратная связь", "url" => ["/manage/feedback/index"]],


                [
                    "label" => "Настройки сайта",
                    "items" => [
                        ["label" => "FAQ", "url" => ["/manage/faq/index"]],
                        "<li class=\"divider\"></li>",
                        ["label" => "Галерея", "url" => ["/manage/gallery/index"]],
                        "<li class=\"divider\"></li>",
                        ["label" => "Отзывы", "url" => ["/manage/review/index"]],
                        "<li class=\"divider\"></li>",
                        ["label" => "Контакты", "url" => ["/manage/contact/index"]],
                        "<li class=\"divider\"></li>",
                        ["label" => "Видео", "url" => ["/manage/video/index"]],
                    ],
                ],
                [
                    "label" => "Клиенты и заказы",
                    "items" => [
                        ["label" => "Клиенты", "url" => ["/manage/client/index"]],
                        "<li class=\"divider\"></li>",
                        ["label" => "Заказы", "url" => ["/manage/order/index"]],
                    ],
                ],
                [
                    "label" => "Настройка формы",
                    "items" => [
                        ["label" => "Обложки", "url" => "/manage/cover/index"],
                        "<li class=\"divider\"></li>",
                        ["label" => "Посты и цены", "url" => "/manage/post-price/index"],
                    ],
                ],
                [
                    "label" => "Профиль",
                    "items" => [
                        ["label" => "Изменить пароль", "url" => "/manage/user/change-password"],
                        "<li class=\"divider\"></li>",
                        (
                            "<li>"
                            . yii\helpers\Html::beginForm(["/auth/logout"], "post")
                            . yii\helpers\Html::submitButton(
                                "Logout (" . Yii::$app->user->identity->email . ")",
                                ["class" => "btn btn-link logout"]
                            )
                            . yii\helpers\Html::endForm()
                            . "</li>"
                        )
                    ],
                ],

            ],
        ]);
        yii\bootstrap\NavBar::end();
        ?>
    <?php endif; ?>
    <div class="container">
        <?= yii\widgets\Breadcrumbs::widget([
            "links" => isset($this->params["breadcrumbs"]) ? $this->params["breadcrumbs"] : [],
            "homeLink" => [
                "label" => "Главная",
                "url" => ["/manage"]
            ]]); ?>
        <?= app\widgets\Alert::widget(); ?>
        <?= $content; ?>
    </div>
</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
