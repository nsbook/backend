<?php /** @var \app\models\Gallery[] $galleries */ ?>
<?php /** @var \app\models\Review[] $reviews */ ?>
<?php /** @var \app\models\Video $video */ ?>
<?php /** @var \app\models\Faq[] $faqs */ ?>
<?php echo $this->render('index/_section1'); ?>
<?php echo $this->render('index/_section2'); ?>
<?php echo $this->render('index/_section3', ['galleries' => $galleries]); ?>
<?php echo $this->render('index/_section4', ['reviews' => $reviews]); ?>
<?php echo $this->render('index/_section5', ['video' => $video]); ?>
<?php echo $this->render('index/_section6', ['faqs' => $faqs]); ?>