<?php /* @var $this \yii\web\View */ ?>
<section class="section-form">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="he">РЕДАКТОР ФОТОКНИГИ</div>
                <?php use yii\bootstrap\ActiveForm;
                use yii\helpers\Html;
                $model = new \app\forms\OrderForm();
                $form = ActiveForm::begin(['id' => 'order-form']); ?>
                <p>Логин в инстаграм, по которому будем делать книгу</p>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">@</span>
                    <?= Html::activeTextInput($model, 'login', $model->input_attributes['login']); ?>
                </div>
                <div style="display: none;">
                    <br/>
                    <img id="account" style="border-radius: 50%;box-shadow: 0 0 0 2px #fff, 0 0 0 3px #d10869;
" />
                    <p id="account_info"></p>
                </div>
                <br/>
                <div class="form-group">
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'id' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>


    </div>
<?php $this->registerJs(
/** @lang JavaScript */
    "
$('#contact-button').on('click', function() {
    var username = $('#insta_login').val();
  $.ajax({
  url:'/site/user-find?username=' + username,
  dataType: 'json',
  success: function(data) {
      if(data['success'] == true) {
          $('#account').attr('src', data['user']['photo']);
          $('#account_info').html(data['user']['username'] + ' - ' + data['user']['fullName']);
          
          $('#account').parent().show();
      }
  }
  });
  return false;
})
", \yii\web\View::POS_READY); ?>
</section>
