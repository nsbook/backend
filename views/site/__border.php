<div id="rec48305617" class="r t-rec t-rec_pt_0 t-rec_pb_0">
    <div class="t029">
        <div class="t029__container t-container">
            <div class="t029__col t-col t-col_8">
                <div class="t029__linewrapper" style="opacity:0.2;">
                    <div class="t029__opacity t029__opacity_left"
                         style="background-image: -moz-linear-gradient(right, #000000, rgba(0,0,0,0)); background-image: -webkit-linear-gradient(right, #000000, rgba(0,0,0,0)); background-image: -o-linear-gradient(right, #000000, rgba(0,0,0,0)); background-image: -ms-linear-gradient(right, #000000, rgba(0,0,0,0));"></div>
                    <div class="t029__line" style="background: #000000;"></div>
                    <div class="t029__opacity t029__opacity_right"
                         style="background-image: -moz-linear-gradient(left, #000000, rgba(0,0,0,0)); background-image: -webkit-linear-gradient(left, #000000, rgba(0,0,0,0)); background-image: -o-linear-gradient(left, #000000, rgba(0,0,0,0)); background-image: -ms-linear-gradient(left, #000000, rgba(0,0,0,0));"></div>
                </div>
            </div>
        </div>
    </div>
</div>