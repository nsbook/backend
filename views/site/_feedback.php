<?php use yii\widgets\ActiveForm; ?>
<?php /** @var \app\forms\FeedbackForm $model */
$model = new \app\forms\FeedbackForm(); ?>
<?php use yii\captcha\Captcha; ?>
<?php echo $this->render('__border'); ?>
    <div id="rec48197906" class="r t-rec" style=" " data-animationappear="off" data-record-type="396">
        <div class="t396">
            <div class="t396__artboard rendered" data-artboard-recid="48197906" style="height: 150px;">
                <div class="t396__elem tn-elem tn-elem__481979061521620007414" data-elem-id="1521620007414"
                     style="top: 40px; left: 762.5px; width: 380px;">
                    <div class="tn-atom" field="tn_text_1521620007414">Написать нам</div>
                </div>
                <div>
                </div>
            </div>
        </div>
    </div>
<?php
$form = ActiveForm::begin(
    [
        'id' => 'feedback-form',
        'method' => 'post',
        'action' => ['site/contact'],
    ]); ?>
    <div class="t-container">
            <?= app\widgets\Alert::widget(); ?>
        <div class="t-col t-col_8 t-prefix_2" style="margin-bottom: 40px;">
            <?= $form->field($model, 'title')->textInput([
                'autofocus' => true,
                'class' => 't186__input t-input js-tilda-rule',
                'style' => 'color:#000000; border:1px solid #000000;',
                'placeholder' => 'Тема обращения'
            ])->label(false); ?>
        </div>
        <div class="t-col t-col_8 t-prefix_2" style="margin-bottom: 40px;">
            <div class="t-select__wrapper ">
                <?= $form->field($model, 'type')->dropDownList(\app\models\Feedback::$types, [
                    'class' => 't-select js-tilda-rule',
                    'style' => 'color:#000000; border:1px solid #000000;'
                ])->label(false); ?>
            </div>
        </div>
        <div class="t-col t-col_8 t-prefix_2" style="margin-bottom: 40px;">
            <?= $form->field($model, 'content')->textarea([
                'class' => 't186__input t-input js-tilda-rule',
                'style' => 'color:#000000; border:1px solid #000000;height:400px;',
                'placeholder' => 'Сообщение'
            ])->label(false); ?>
        </div>
        <div class="t-col t-col_8 t-prefix_2" style="margin-bottom: 40px;">
            <?= $form->field($model, 'verifyCode', [
            ])->widget(Captcha::class, [
                'options' => [
                    'class' => 't186__input t-input js-tilda-rule',
                    'style' => 'color:#000000; border:1px solid #000000;',
                    'placeholder' => 'Код проверки'
                ]
            ])->label(false); ?>
        </div>
        <div class="t678">
            <div class="t-form__submit">
                <button type="submit" class="t-submit"
                        style="color:#000000;border:1px solid #000000;background-color:#ffe100;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.3);">
                    Отправить
                </button>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>