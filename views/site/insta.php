<?php /** @var \app\controllers\SiteController $this */ ?>
<?php $this->title = 'Книга из instagram'; ?>
<?php echo $this->render('__border'); ?>
<?php  echo $this->render('insta/_section1', ['user' => $user]); ?>
<?php if (!is_null(Yii::$app->request->getQueryParam('login')) && is_null($user)):?>
<div style="margin: 40px 0px; text-align: center;">
    <h3>Ничего не найдено</h3>
</div>
<?php endif;?>
<?php if (!is_null($user)): echo $this->render('insta/_section2', ['user' => $user]); endif; ?>

