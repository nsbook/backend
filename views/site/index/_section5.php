<?php /** @var \app\models\Video $video */?>
<div id="rec47015283" class="r t-rec t-rec_pt_60 t-rec_pt-res-480_30 t-rec_pb_30 t-rec_pb-res-480_30">
    <div class="t051">
        <div class="t-container">
            <div class="t-col t-col_12 ">
                <div class="t051__text t-text t-text_md">
                    <div id="rec47015283-text">
                        <strong>Создайте фотокнигу онлайн не выходя из дома!</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rec47015284" class="r t-rec t-rec_pt_30 t-rec_pt-res-480_0 t-rec_pb_60 t-rec_pb-res-480_30">
    <div class="t121">
        <center>
            <div class="t-width t-width_10" style="height:540px;">
                <?php if ($video): echo $video->content; endif;?>
            </div>
        </center>
    </div>
</div>
<div id="rec47475513" class="r t-rec">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="47475513">
            <div class='t396__elem tn-elem tn-elem__474755131520367851657' data-elem-id='1520367851657'>
                <a class='tn-atom' href="#rec47015278">Создать фотобук</a>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs(/** @lang JavaScript */
    "        
t121_setHeight(\"47015284\");
$(window).load(function () {
    t121_setHeight(\"47015284\");
});
$(window).resize(function () {
    t121_setHeight(\"47015284\");
});
$('.t121').bind('displayChanged', function () {
    t121_setHeight(\"47015284\");
});
        ", \yii\web\View::POS_READY); ?>