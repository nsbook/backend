<div id="rec47015281" class="r t-rec t-rec_pt_60 t-rec_pt-res-480_30 t-rec_pb_60 t-rec_pb-res-480_0">
    <div class="t533">
        <div class="t-section__container t-container">
            <div class="t-col t-col_12">
                <div class="t-section__topwrapper t-align_center">
                    <div class="t-section__title t-title t-title_xs">
                        <div>Мы любим свою работу <br/></div>
                    </div>
                    <div class="t-section__descr t-descr t-descr_xl t-margin_auto">
                        <div>и убеждаемся в этом каждый раз, когда получаем от вас отзывы!</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="t-container">
            <div class="t533__row t533__separator">
                <?php if (isset($reviews)): ?>
                    <?php foreach ($reviews as $review): ?>
                        <div class="t533__col t-col t-col_4 ">
                            <div class="t533__content">
                                <div class="t533__textwrapper t-align_center" style=" border: 0px solid #000; ">
                                    <div class="t533__title t-name t-name_xs">
                                        <?php echo $review->title; ?>
                                    </div>
                                    <div class="t533__descr t-text t-text_xs">
                                        <div style="text-align:left;" data-customstyle="yes">
                                            <?php echo $review->content; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="t533__imgwrapper">
                                    <div class="t533__img t-bgimg"
                                         data-original="https://static.tildacdn.com/tild3665-6432-4238-b631-633038383731/2new_1456175606.jpg"
                                         bgimgfield="li_img__1478014727987"
                                         style=" background-image: url('https://static.tildacdn.com/tild3665-6432-4238-b631-633038383731/-/resize/20x/2new_1456175606.jpg');">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
<div id="rec47015282" class="r t-rec" style=" " data-animationappear="off" data-record-type="396">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="47015282">
            <div class='t396__elem tn-elem tn-elem__470152821520367851657' data-elem-id='1520367851657'>
                <a class='tn-atom' href="#rec47015278">Создать фотобук</a>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(/** @lang JavaScript */
    "t396_init('47015282'); t533_equalHeight('47015281');t396_init('47475513');", \yii\web\View::POS_READY); ?>
<?php $this->registerJs(/** @lang JavaScript */
    "t533_equalHeight('47015281');", \yii\web\View::POS_LOAD); ?>
<?php $this->registerJs(/** @lang JavaScript */
    " 
var t533_doResize;
$(window).resize(function () {
    if (t533_doResize) clearTimeout(t533_doResize);
    t533_doResize = setTimeout(function () {
        t533_equalHeight('47015281');
    }, 200);
});
        ", \yii\web\View::POS_READY); ?>
