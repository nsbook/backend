<?php /** @var \app\models\Faq[] $faqs */?>
<div id="rec47425984" class="r t-rec">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="47425984">
            <div class="t396__carrier" data-artboard-recid="47425984"></div>
            <div class="t396__filter" data-artboard-recid="47425984"></div>
            <div class='t396__elem tn-elem tn-elem__474259841520923609101' data-elem-id='1520923609101'>
                <div class='tn-atom'>FAQ</div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */ "t396_init('47425984');", \yii\web\View::POS_READY); ?>
</div>
<?php if ($faqs): ?>
<div id="rec47421036" class="r t-rec t-rec_pt_15 t-rec_pt-res-480_15 t-rec_pb_60 t-rec_pb-res-480_60">
    <div class="t585">
        <div class="t-container">
            <?php $i = 0;?>
            <?php foreach (\app\models\Faq::$categories as $categoryId => $category): ?>
            <?php $i++; ?>
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t585__accordion">
                        <div class="t585__wrapper">
                            <div class="t585__header" style=" border-top: 1px solid #eee;">
                                <div class="t585__title t-name t-name_xl" field="li_title__1480611044356" style="">
                                    <div style="font-family:'GothaPro';" data-customstyle="yes">
                                        <span style="font-weight: 700;"><?php echo $category; ?></span>
                                    </div>
                                </div>
                                <div class="t585__icon">
                                    <div class="t585__lines">
                                        <svg width="24px" height="24px" viewBox="0 0 24 24"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
                                               stroke-linecap="square">
                                                <g transform="translate(1.000000, 1.000000)" stroke="#222222">
                                                    <path d="M0,11 L22,11"></path>
                                                    <path d="M11,0 L11,22"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="t585__circle" style="background-color: transparent;"></div>
                                </div>
                                <div class="t585__icon t585__icon-hover">
                                    <div class="t585__lines">
                                        <svg width="24px" height="24px" viewBox="0 0 24 24"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
                                               stroke-linecap="square">
                                                <g transform="translate(1.000000, 1.000000)" stroke="#222222">
                                                    <path d="M0,11 L22,11"></path>
                                                    <path d="M11,0 L11,22"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="t585__circle" style="background-color: #eee;"></div>
                                </div>
                            </div>
                            <div class="t585__content">
                                <div class="t585__textwrapper">
                                    <div class="t585__text t-descr t-descr_xs" field="li_descr__1480611044356" style="">
                                        <div style="font-size:18px;" data-customstyle="yes">
                                            <?php $n = 0; ?>
                                            <?php foreach ($faqs as $faq): ?>
                                            <?php if ($faq->category == $categoryId): ?>
                                                <?php $n++; ?>
                                            <a class="show-popup" href="#popup:infoblock<?= $i; ?>_<?= $n; ?>" style="color:#0000ff !important;text-decoration: none;border-bottom: 1px solid #0011ff;box-shadow: inset 0px -0px 0px 0px #0011ff;-webkit-box-shadow: inset 0px -0px 0px 0px #0011ff;-moz-box-shadow: inset 0px -0px 0px 0px #0011ff;"><?= $faq->question; ?></a>
                                                    <?php $this->registerJs(/** @lang JavaScript */ "
                                                    $(\"#rec47421432\").attr('data-animationappear', 'off');
            $(\"#rec47421432\").css('opacity', '1');
            setTimeout(function () {
                t390_initPopup('" . $i . $n . "');
            }, 500);", \yii\web\View::POS_READY); ?>
                                            <div id="rec<?= $i . $n; ?>" class="r t-rec" style=" " data-record-type="390">
                                                <div class="t390">
                                                    <div id="popup:infoblock<?= $i; ?>_<?= $n; ?>" class="t-popup" data-tooltip-hook="#popup:infoblock<?= $i; ?>_<?= $n; ?>">
                                                        <div class="t-popup__close">
                                                            <div class="t-popup__close-wrapper">
                                                                <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
                                                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                                                                        <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                                                              x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                                                                        <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                                                              x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
                                                            <div class="t390__wrapper t-align_center">
                                                                <div class="t390__title t-heading t-heading_lg"><?= $faq->question; ?></div>
                                                                <div class="t390__descr t-descr t-descr_xs">
                                                                    <div style="text-align:left;" data-customstyle="yes"><?= $faq->answer; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */ "t585_init('47421036');", \yii\web\View::POS_READY); ?>
</div>
<?php endif;?>
<div id="rec47475486" class="r t-rec">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="47475486">
            <div class='t396__elem tn-elem tn-elem__474754861520367851657' data-elem-id='1520367851657'>
                <a class='tn-atom' href="/site/contact">Написать нам</a>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */ "t396_init('47475486');", \yii\web\View::POS_READY); ?>
</div>

