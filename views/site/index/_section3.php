<div id="rec47015279" class="r t-rec t-rec_pt_45 t-rec_pb_45">
    <div class="t051">
        <div class="t-container">
            <div class="t-col t-col_12 ">
                <div class="t051__text t-text t-text_md">
                    <div>
                        <strong>
                            <span>Галерея</span>
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rec47015280" class="r t-rec t-rec_pt_0 t-rec_pb_60">
    <div class="t603">
        <div class="t603__container t-container_100">
            <?php if (isset($galleries)): ?>
            <?php foreach ($galleries as $gallery): ?>
                <div class="t603__tile t603__tile_25" itemscope itemtype="http://schema.org/ImageObject">
                    <div class="t603__blockimg t603__blockimg_4-3 t-bgimg t-animate"
                         data-animate-style="fadein"
                         data-animate-chain="yes" bgimgfield="gi_img__0"
                         data-zoom-target="0" data-zoomable="yes"
                         data-img-zoom-url="<?= $gallery->url; ?>"
                         data-original="<?= $gallery->url; ?>"
                    style="background: url('<?= $gallery->url; ?>') center center / cover no-repeat;}">
                        <meta itemprop="image" content="<?= $gallery->url; ?>">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
