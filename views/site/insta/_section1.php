<?php /** @var \InstagramScraper\Model\Account $user */ ?>
<div id="rec48197906" class="r t-rec">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48197906">
            <div class="t396__carrier" data-artboard-recid="48197906"></div>
            <div class="t396__filter" data-artboard-recid="48197906"></div>
            <div class='t396__elem tn-elem tn-elem__481979061521540249895' data-elem-id='1521540249895'>
                <div class='tn-atom'>1. Введите логин Instagram (по желанию)</div>
            </div>
            <div class="t396__elem tn-elem tn-elem__481979061521540316248" data-elem-id="1521540316248">
                <a class="tn-atom" href="#popup:?1">?</a>
            </div>
            <div class='t396__elem tn-elem tn-elem__481979061521618994824' data-elem-id='1521618994824'>
                <div class='tn-atom'>
                    Укажите логин в Instagram, даты и количество постов. Остальное сделаем мы - ваша готовая Insta-книга
                    будет передана в службу доставки через 26 часов.
                </div>
            </div>
            <div class='t396__elem tn-elem tn-elem__481979061521620007414' data-elem-id='1521620007414'>
                <div class='tn-atom'>РЕДАКТОР ФОТОКНИГИ</div>
            </div>
        </div>
    </div>
</div>
<div id="rec48199312" class="r t-rec" style=" " data-animationappear="off" data-record-type="303">
    <div class="t300" data-tooltip-hook="?1" data-tooltip-id="48199312" data-tooltip-position="bottom-right">
        <div class="t300__content">
            <div class="t300__content-title">Логин Instagram</div>
            <div class="t300__content-text">Если вы хотите, чтобы мы напечатали фото из вашего профиля, то введите
                логин (без ссылки https://www.instagram.com/)
            </div>
        </div>
    </div>
</div>
<div id="rec48201119" class="r t-rec" style=" " data-record-type="390">
    <div class="t390">
        <div class="t-popup" data-tooltip-hook="#popup:?1">
            <div class="t-popup__close">
                <div class="t-popup__close-wrapper">
                    <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;"><img
                        class="t390__img t-img"
                        src="https://static.tildacdn.com/tild3264-3338-4361-b130-333132313633/-/empty/inst1.jpg"
                        data-original="https://static.tildacdn.com/tild3264-3338-4361-b130-333132313633/inst1.jpg"
                        imgfield="img">
                <div class="t390__wrapper t-align_center">
                    <div class="t390__title t-heading t-heading_lg">
                        <div style="text-align:left;" data-customstyle="yes">Логин Instagram</div>
                    </div>
                    <div class="t390__descr t-descr t-descr_xs">
                        <div style="text-align:left;" data-customstyle="yes">Если вы хотите, чтобы мы напечатали
                            фото из вашего профиля, то введите логин (без ссылки https://www.instagram.com/)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rec48172598" class="r t-rec t-rec_pt_15 t-rec_pt-res-480_15 t-rec_pb_15 t-rec_pb-res-480_15"
     style="padding-top:15px;padding-bottom:15px; " data-record-type="210">
    <div class="t186">
        <form id="form48172598" name='form48172598' role="form" action='' method='GET' data-formactiontype="0"
              data-inputbox=".t186__blockinput" class="js-form-proccess " data-tilda-captchakey="">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t186__wrapper">
                        <div class="t186__blockinput">
                            <input type="text"
                                   name="login"
                                   class="t186__input t-input js-tilda-rule "
                                   value="<?= \yii\helpers\Html::encode(Yii::$app->request->getQueryParam('login')); ?>"
                                   placeholder="Введите логин Instagram"
                                   style="color:#000000; border:1px solid #000000; ">
                        </div>
                        <div class="t186__blockbutton">
                            <button id="search-insta" type="submit" class="t-submit"
                                    style="color:#000000;border:1px solid #000000;background-color:#ffe100;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;font-weight:700;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.3);">
                                Применить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->registerJs(/** @lang JavaScript */
    "t396_init('48197906');", \yii\web\View::POS_READY); ?>
<?php $this->registerJs(/** @lang JavaScript */
    "t396_init('48197906');", \yii\web\View::POS_READY); ?>

<?php $this->registerJs(/** @lang JavaScript */
    "          $(\"#rec48201119\").attr('data-animationappear', 'off');
            $(\"#rec48201119\").css('opacity', '1');
            setTimeout(function () {
                t390_initPopup('48201119');
            }, 500);", \yii\web\View::POS_READY); ?>
