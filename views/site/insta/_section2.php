<?php /** @var \app\models\InstaAccount $user */ ?>
<div id="rec48302539" class="r t-rec" data-animationappear="off" data-record-type="396">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48302539">
            <div class='t396__elem tn-elem tn-elem__483025391521619720018' data-elem-id='1521619720018'>
                <div class='tn-atom'>
                    <img class='tn-atom__img t-img' style="border-radius: 50%; box-shadow: 0 0 0 3px #fff, 0 0 0 5px #d10869;" data-original='<?php echo $user->profile_pic_url; ?>' imgfield='tn_img_1521619720018'/>
                </div>
            </div>
            <div class='t396__elem tn-elem tn-elem__483025391521619758530' data-elem-id='1521619758530'>
                <div class='tn-atom' field='tn_text_1521619758530'>
                    <strong><?= $user->username; ?></strong> - <?= $user->full_name; ?>
                    <p><?= $user->biography; ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48302539');", \yii\web\View::POS_READY); ?>
</div>
<input type="hidden" id="userId" value="<?= $user->insta_id; ?>">
<div id="rec48203744" class="r t-rec" data-animationappear="off" data-record-type="396">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48203744">
            <div class="t396__carrier" data-artboard-recid="48203744"></div>
            <div class="t396__filter" data-artboard-recid="48203744"></div>
            <div class='t396__elem tn-elem tn-elem__482037441521540249895' data-elem-id='1521540249895'>
                <div class='tn-atom' field='tn_text_1521540249895'>2. Выберите дату постов (по желанию)</div>
            </div>
            <div class='t396__elem tn-elem tn-elem__482037441521540316248' data-elem-id='1521540316248'>
                <a class='tn-atom' href="#popup:?2">?</a></div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48203744');", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48203899" class="r t-rec" data-animationappear="off" data-record-type="303">
    <div class="t300" data-tooltip-hook="?2" data-tooltip-id="48203899" data-tooltip-position="bottom-right">
        <div class="t300__content">
            <div class="t300__content-title">Дата постов</div>
            <div class="t300__content-text">Мы выберем только те посты, которые вписываются в выбранные временные
                рамки
            </div>
        </div>
    </div>
</div>
<div id="rec48204107" class="r t-rec" data-record-type="390">
    <div class="t390">
        <div class="t-popup" data-tooltip-hook="#popup:?2">
            <div class="t-popup__close">
                <div class="t-popup__close-wrapper">
                    <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
                <div class="t390__wrapper t-align_center">
                    <div class="t390__title t-heading t-heading_lg">
                        <div style="text-align:left;" data-customstyle="yes">Дата постов</div>
                    </div>
                    <div class="t390__descr t-descr t-descr_xs">
                        <div style="text-align:left;" data-customstyle="yes">Мы выберем только те посты, которые
                            вписываются в выбранные временные рамки
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "            $(\"#rec48204107\").attr('data-animationappear', 'off');
            $(\"#rec48204107\").css('opacity', '1');
            setTimeout(function () {
                t390_initPopup('48204107');
            }, 500);", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48173385" class="r t-rec t-rec_pt_15 t-rec_pt-res-480_15 t-rec_pb_30 t-rec_pb-res-480_15">
    <div class="t690">
        <div class="t-container">
            <div class="t-col t-col_8 t-prefix_2">
                <form id="form48173385" name='form48173385' role="form" action='' method='POST'
                      data-formactiontype="0" data-inputbox=".t-input-group"
                      class="t-form js-form-proccess t-form_inputs-total_2 " data-success-callback="t690_onSuccess">
                    <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>
                    <div class="t-form__inputsbox">
                        <div class="t-input-group t-input-group_da" data-input-lid="1521528243978">
                            <div class="t-input-block">
                                <div class="t-datepicker__wrapper">
                                    <?php $dateFrom = new DateTime('first day of this month'); ?>
                                    <input type="text" name="data1" id="dateFrom"
                                           class="t-input t-datepicker js-tilda-rule js-tilda-mask "
                                           value="<?= $dateFrom->format('d.m.Y'); ?>" placeholder="с 01.01.2018"
                                           data-tilda-rule="date"
                                           data-tilda-dateformat="DD-MM-YYYY"
                                           data-tilda-datediv="dot"
                                           data-tilda-mask="99.99.9999"
                                           style="color:#000000; border:1px solid #000000; ">
                                    <svg class="t-datepicker__icon " xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 69.5 76.2" style="width:25px;">
                                        <path d="M9.6 42.9H21V31.6H9.6v11.3zm3-8.3H18v5.3h-5.3v-5.3zm16.5 8.3h11.3V31.6H29.1v11.3zm3-8.3h5.3v5.3h-5.3v-5.3zM48 42.9h11.3V31.6H48v11.3zm3-8.3h5.3v5.3H51v-5.3zM9.6 62H21V50.6H9.6V62zm3-8.4H18V59h-5.3v-5.4zM29.1 62h11.3V50.6H29.1V62zm3-8.4h5.3V59h-5.3v-5.4zM48 62h11.3V50.6H48V62zm3-8.4h5.3V59H51v-5.4z"/>
                                        <path d="M59.7 6.8V5.3c0-2.9-2.4-5.3-5.3-5.3s-5.3 2.4-5.3 5.3v1.5H40V5.3C40 2.4 37.6 0 34.7 0s-5.3 2.4-5.3 5.3v1.5h-9.1V5.3C20.3 2.4 18 0 15 0c-2.9 0-5.3 2.4-5.3 5.3v1.5H0v69.5h69.5V6.8h-9.8zm-7.6-1.5c0-1.3 1-2.3 2.3-2.3s2.3 1 2.3 2.3v7.1c0 1.3-1 2.3-2.3 2.3s-2.3-1-2.3-2.3V5.3zm-19.7 0c0-1.3 1-2.3 2.3-2.3S37 4 37 5.3v7.1c0 1.3-1 2.3-2.3 2.3s-2.3-1-2.3-2.3V5.3zm-19.6 0C12.8 4 13.8 3 15 3c1.3 0 2.3 1 2.3 2.3v7.1c0 1.3-1 2.3-2.3 2.3-1.3 0-2.3-1-2.3-2.3V5.3zm53.7 67.9H3V9.8h6.8v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h9.1v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h9.1v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h6.8l-.1 63.4z"/>
                                    </svg>
                                </div>
                                <?php $this->registerJs(/** @lang JavaScript */
                                    "                                                 try {
                                            setTimeout(function () {
                                                t_datepicker_init('48173385', '1521528243978');
                                            }, 500);
                                        } catch (err) {
                                        }", \yii\web\View::POS_READY); ?>
                                <div class="t-input-error"></div>
                            </div>
                        </div>
                        <div class="t-input-group t-input-group_da" data-input-lid="1521528291248">
                            <div class="t-input-block">
                                <div class="t-datepicker__wrapper">
                                    <input type="text" name="data2" id="dateTo"
                                           class="t-input t-datepicker js-tilda-rule js-tilda-mask "
                                           value="<?= date('d.m.Y') ?>" placeholder="по 01.05.2018"
                                           data-tilda-rule="date"
                                           data-tilda-dateformat="DD-MM-YYYY"
                                           data-tilda-datediv="dot"
                                           data-tilda-mask="99.99.9999"
                                           style="color:#000000; border:1px solid #000000; ">
                                    <svg class="t-datepicker__icon " xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 69.5 76.2" style="width:25px;">
                                        <path d="M9.6 42.9H21V31.6H9.6v11.3zm3-8.3H18v5.3h-5.3v-5.3zm16.5 8.3h11.3V31.6H29.1v11.3zm3-8.3h5.3v5.3h-5.3v-5.3zM48 42.9h11.3V31.6H48v11.3zm3-8.3h5.3v5.3H51v-5.3zM9.6 62H21V50.6H9.6V62zm3-8.4H18V59h-5.3v-5.4zM29.1 62h11.3V50.6H29.1V62zm3-8.4h5.3V59h-5.3v-5.4zM48 62h11.3V50.6H48V62zm3-8.4h5.3V59H51v-5.4z"/>
                                        <path d="M59.7 6.8V5.3c0-2.9-2.4-5.3-5.3-5.3s-5.3 2.4-5.3 5.3v1.5H40V5.3C40 2.4 37.6 0 34.7 0s-5.3 2.4-5.3 5.3v1.5h-9.1V5.3C20.3 2.4 18 0 15 0c-2.9 0-5.3 2.4-5.3 5.3v1.5H0v69.5h69.5V6.8h-9.8zm-7.6-1.5c0-1.3 1-2.3 2.3-2.3s2.3 1 2.3 2.3v7.1c0 1.3-1 2.3-2.3 2.3s-2.3-1-2.3-2.3V5.3zm-19.7 0c0-1.3 1-2.3 2.3-2.3S37 4 37 5.3v7.1c0 1.3-1 2.3-2.3 2.3s-2.3-1-2.3-2.3V5.3zm-19.6 0C12.8 4 13.8 3 15 3c1.3 0 2.3 1 2.3 2.3v7.1c0 1.3-1 2.3-2.3 2.3-1.3 0-2.3-1-2.3-2.3V5.3zm53.7 67.9H3V9.8h6.8v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h9.1v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h9.1v2.6c0 2.9 2.4 5.3 5.3 5.3s5.3-2.4 5.3-5.3V9.8h6.8l-.1 63.4z"/>
                                    </svg>
                                </div>
                                <?php $this->registerJs(/** @lang JavaScript */
                                    "                                     try {
                                            setTimeout(function () {
                                                t_datepicker_init('48173385', '1521528291248');
                                            }, 500);
                                        } catch (err) {
                                        }", \yii\web\View::POS_READY); ?>
                                <div class="t-input-error"></div>
                            </div>
                        </div>
                        <div class="t-form__errorbox-middle">
                            <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                <div class="t-form__errorbox-text t-text t-text_md">
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></div>
                            </div>
                        </div>
                        <div class="t-form__submit">
                            <button type="submit" class="t-submit get-images"
                                    style="color:#000000;border:1px solid #000000;background-color:#ffe100;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.3);">
                                Применить
                            </button>
                        </div>
                    </div>
                    <div class="t-form__errorbox-bottom">
                        <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                            <div class="t-form__errorbox-text t-text t-text_md">
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="rec48204974" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48204974" data-artboard-height="140"
             data-artboard-height-res-960="100" data-artboard-height-res-320="80" data-artboard-height_vh=""
             data-artboard-valign="center">
            <div class="t396__carrier" data-artboard-recid="48204974"></div>
            <div class="t396__filter" data-artboard-recid="48204974"></div>
            <div class='t396__elem tn-elem tn-elem__482049741521540249895' data-elem-id='1521540249895'
                 data-elem-type='text' data-field-top-value="70" data-field-top-res-960-value="49"
                 data-field-top-res-640-value="40" data-field-top-res-480-value="40"
                 data-field-top-res-320-value="42" data-field-left-value="220" data-field-left-res-960-value="170"
                 data-field-left-res-640-value="40" data-field-left-res-480-value="25"
                 data-field-left-res-320-value="15" data-field-width-value="560"
                 data-field-width-res-480-value="470" data-field-width-res-320-value="300"
                 data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                 data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                 data-field-widthunits-value="">
                <div class='tn-atom' field='tn_text_1521540249895'>3. <strong>Укажите количество постов/фото
                        (обязательно)</strong></div>
            </div>
            <div class='t396__elem tn-elem tn-elem__482049741521540316248' data-elem-id='1521540316248'
                 data-elem-type='button' data-field-top-value="77" data-field-top-res-960-value="56"
                 data-field-top-res-640-value="47" data-field-top-res-480-value="43"
                 data-field-top-res-320-value="39" data-field-left-value="758" data-field-left-res-960-value="610"
                 data-field-left-res-640-value="480" data-field-left-res-480-value="402"
                 data-field-left-res-320-value="285" data-field-height-value="20" data-field-width-value="20"
                 data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                 data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                 data-field-widthunits-value=""><a class='tn-atom' href="#popup:?3">?</a></div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48204974');", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48204956" class="r t-rec" style=" " data-animationappear="off" data-record-type="303">
    <div class="t300" data-tooltip-hook="?3" data-tooltip-id="48204956" data-tooltip-position="bottom-right">
        <div class="t300__content">
            <div class="t300__content-title">Количество постов</div>
            <div class="t300__content-text">Если вы пока не определились сколько постов хотите напечатать, то
                поставьте 100 постов, позже вы сможете добавить еще и загрузить свои фото.
            </div>
        </div>
    </div>
</div>
<div id="rec48204814" class="r t-rec" style=" " data-record-type="390">
    <div class="t390">
        <div class="t-popup" data-tooltip-hook="#popup:?3">
            <div class="t-popup__close">
                <div class="t-popup__close-wrapper">
                    <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
                <div class="t390__wrapper t-align_center">
                    <div class="t390__title t-heading t-heading_lg">
                        <div style="text-align:left;" data-customstyle="yes">Количество постов</div>
                    </div>
                    <div class="t390__descr t-descr t-descr_xs">
                        <div style="text-align:left;" data-customstyle="yes">Если вы пока не определились сколько
                            постов хотите напечатать, то поставьте 100 постов, позже вы сможете добавить еще и
                            загрузить свои фото.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "            $(\"#rec48204814\").attr('data-animationappear', 'off');
            $(\"#rec48204814\").css('opacity', '1');
            setTimeout(function () {
                t390_initPopup('48204814');
            }, 500);", \yii\web\View::POS_READY); ?>

</div>
<div id="rec48174213" class="r t-rec t-rec_pt_15 t-rec_pt-res-480_15 t-rec_pb_30 t-rec_pb-res-480_15"
     style="padding-top:15px;padding-bottom:30px; " data-animationappear="off" data-record-type="690"><!-- t690 -->
    <div class="t690">
        <div class="t-container">
            <div class="t-col t-col_8 t-prefix_2">
                <form id="form48174213" name='form48174213' role="form" action='' method='POST'
                      data-formactiontype="0" data-inputbox=".t-input-group"
                      class="t-form js-form-proccess t-form_inputs-total_1 " data-success-callback="t690_onSuccess">
                    <!-- NO ONE SERVICES CONNECTED -->
                    <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>
                    <div class="t-form__inputsbox">
                        <div class="t-input-group t-input-group_sb" data-input-lid="1521528691552">
                            <div class="t-input-block">
                                <div class="t-select__wrapper ">
                                    <select name="post" class="t-select js-tilda-rule " style="color:#000000; border:1px solid #000000; ">
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="200">200</option>
                                        <option value="300">300</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                    </select></div>
                                <div class="t-input-error"></div>
                            </div>
                        </div>
                        <div class="t-form__errorbox-middle">
                            <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                <div class="t-form__errorbox-text t-text t-text_md"><p
                                            class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p></div>
                            </div>
                        </div>
                        <div class="t-form__submit">
                            <button type="submit" class="t-submit"
                                    style="color:#000000;border:1px solid #000000;background-color:#ffe100;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.3);">
                                Применить
                            </button>
                        </div>
                    </div>
                    <div class="t-form__errorbox-bottom">
                        <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                            <div class="t-form__errorbox-text t-text t-text_md"><p
                                        class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="rec48205748" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48205748" data-artboard-height="140"
             data-artboard-height-res-960="100" data-artboard-height-res-320="80" data-artboard-height_vh=""
             data-artboard-valign="center">
            <div class="t396__carrier" data-artboard-recid="48205748"></div>
            <div class="t396__filter" data-artboard-recid="48205748"></div>
            <div class='t396__elem tn-elem tn-elem__482057481521540249895' data-elem-id='1521540249895'
                 data-elem-type='text' data-field-top-value="70" data-field-top-res-960-value="49"
                 data-field-top-res-640-value="40" data-field-top-res-480-value="40"
                 data-field-top-res-320-value="42" data-field-left-value="220" data-field-left-res-960-value="170"
                 data-field-left-res-640-value="40" data-field-left-res-480-value="25"
                 data-field-left-res-320-value="15" data-field-width-value="560"
                 data-field-width-res-480-value="470" data-field-width-res-320-value="300"
                 data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                 data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                 data-field-widthunits-value="">
                <div class='tn-atom' field='tn_text_1521540249895'>4. Выберите настройки (по
                    желанию)<strong></strong></div>
            </div>
            <div class='t396__elem tn-elem tn-elem__482057481521540316248' data-elem-id='1521540316248'
                 data-elem-type='button' data-field-top-value="77" data-field-top-res-960-value="56"
                 data-field-top-res-640-value="47" data-field-top-res-480-value="43"
                 data-field-top-res-320-value="39" data-field-left-value="621" data-field-left-res-960-value="570"
                 data-field-left-res-640-value="440" data-field-left-res-480-value="372"
                 data-field-left-res-320-value="255" data-field-height-value="20" data-field-width-value="20"
                 data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                 data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                 data-field-widthunits-value=""><a class='tn-atom' href="#popup:?4">?</a></div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48205748');", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48205741" class="r t-rec" style=" " data-animationappear="off" data-record-type="303">
    <div class="t300" data-tooltip-hook="?4" data-tooltip-id="48205741" data-tooltip-position="bottom-right">
        <div class="t300__content">
            <div class="t300__content-title">Настройки</div>
            <div class="t300__content-text"><strong>Печатать посты по дате (по порядку</strong><strong>) </strong>-
                сначала будем печатать новые посты<br/><strong>Выбирать посты с наибольшим количеством
                    лайков </strong>- наш алгоритм подберет самые популярные посты из вашего аккаунта<br/><strong>Использовать
                    все фотографии из поста (карусели) </strong>- будут напечатаны все посты из
                карусели<br/><strong>Печатать геоданные</strong><strong> </strong>- будут напечатаны геоданные
                (места)<br/><strong>Печатать дату постов </strong><br/><strong>Печатать количество
                    лайков</strong><br/><strong>Печатать подпись постов </strong>- сможем напечатать 3000 символов
                (с пробелами)<br/><strong>Не печатать хештеги </strong>- фразы с <strong>#</strong> не будут
                напечатаны<br/><br/></div>
        </div>
    </div>
</div>
<div id="rec48205731" class="r t-rec" style=" " data-record-type="390">
    <div class="t390">
        <div class="t-popup" data-tooltip-hook="#popup:?4">
            <div class="t-popup__close">
                <div class="t-popup__close-wrapper">
                    <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
                            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) "
                                  x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
                <div class="t390__wrapper t-align_center">
                    <div class="t390__title t-heading t-heading_lg">
                        <div style="text-align:left;" data-customstyle="yes">Настройки</div>
                    </div>
                    <div class="t390__descr t-descr t-descr_xs">
                        <div style="text-align:left;" data-customstyle="yes"><strong>Печатать посты по дате (по
                                порядку</strong><strong>)</strong> - сначала будем печатать новые посты<br/><strong>Выбирать
                                посты с наибольшим количеством лайков</strong> - наш алгоритм подберет самые
                            популярные посты из вашего аккаунта<br/><strong>Использовать все фотографии из поста
                                (карусели)</strong> - будут напечатаны все посты из карусели<br/><strong>Печатать
                                геоданные</strong><strong></strong> - будут напечатаны геоданные
                            (места)<br/><strong>Печатать дату постов</strong><br/><strong>Печатать количество
                                лайков</strong><br/><strong>Печатать подпись постов</strong> - сможем напечатать
                            3000 символов (с пробелами)<br/><strong>Не печатать хештеги</strong> - фразы с
                            <strong>#</strong> не будут напечатаны
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "           $(\"#rec48205731\").attr('data-animationappear', 'off');
            $(\"#rec48205731\").css('opacity', '1');
            setTimeout(function () {
                t390_initPopup('48205731');
            }, 500);", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48183699" class="r t-rec t-rec_pt_15 t-rec_pb_60" style="padding-top:15px;padding-bottom:60px; "
     data-animationappear="off" data-record-type="678"><!-- t678 -->
    <div class="t678 ">
        <div class="t-container">
            <div class="t-col t-col_8 t-prefix_2">
                <div>
                    <form id="form48183699" name='form48183699' role="form" action='' method='POST'
                          data-formactiontype="0" data-inputbox=".t-input-group"
                          class="t-form js-form-proccess t-form_inputs-total_6 "
                          data-success-callback="t678_onSuccess"> <!-- NO ONE SERVICES CONNECTED -->
                        <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>
                        <div class="t-form__inputsbox">
                            <div class="t-input-group t-input-group_cb" data-input-lid="1521534440143">
                                <div class="t-input-block"><label class="t-checkbox__control t-text t-text_xs"
                                                                  style=""><input type="checkbox" name="chek1"
                                                                                  value="yes"
                                                                                  class="t-checkbox js-tilda-rule"
                                                                                  checked>
                                        <div class="t-checkbox__indicator"></div>
                                        Выбирать посты с наибольшим количеством лайков</label>
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_cb" data-input-lid="1521539841768">
                                <div class="t-input-block"><label class="t-checkbox__control t-text t-text_xs"
                                                                  style=""><input type="checkbox" name="Checkbox"
                                                                                  value="yes"
                                                                                  class="t-checkbox js-tilda-rule"
                                                                                  checked>
                                        <div class="t-checkbox__indicator"></div>
                                        Печатать подпись постов</label>
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_cb" data-input-lid="1521539890195">
                                <div class="t-input-block"><label class="t-checkbox__control t-text t-text_xs"
                                                                  style=""><input type="checkbox" name="Checkbox_2"
                                                                                  value="yes"
                                                                                  class="t-checkbox js-tilda-rule"
                                                                                  checked>
                                        <div class="t-checkbox__indicator"></div>
                                        Печатать геоданные</label>
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_cb" data-input-lid="1521539929108">
                                <div class="t-input-block"><label class="t-checkbox__control t-text t-text_xs"
                                                                  style=""><input type="checkbox" name="Checkbox_3"
                                                                                  value="yes"
                                                                                  class="t-checkbox js-tilda-rule"
                                                                                  checked>
                                        <div class="t-checkbox__indicator"></div>
                                        Печатать дату постов</label>
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-input-group t-input-group_cb" data-input-lid="1521539964458">
                                <div class="t-input-block"><label class="t-checkbox__control t-text t-text_xs"
                                                                  style=""><input type="checkbox" name="Checkbox_4"
                                                                                  value="yes"
                                                                                  class="t-checkbox js-tilda-rule"
                                                                                  checked>
                                        <div class="t-checkbox__indicator"></div>
                                        Печатать количество лайков</label>
                                    <div class="t-input-error"></div>
                                </div>
                            </div>
                            <div class="t-form__errorbox-middle">
                                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                    <div class="t-form__errorbox-text t-text t-text_md"><p
                                                class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="t-form__errorbox-bottom">
                            <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                                <div class="t-form__errorbox-text t-text t-text_md"><p
                                            class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                                    <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="rec48171082" class="r t-rec" style="display:none;" data-animationappear="off" data-record-type="396">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48171082">
            <div class="t396__carrier" data-artboard-recid="48171082"></div>
            <div class="t396__filter" data-artboard-recid="48171082"></div>
            <div class='t396__elem tn-elem tn-elem__481710821519837491780' data-elem-id='1519837491780'>
                <div class='tn-atom' field='tn_text_1519837491780'>
                    <ul>
                        <li>Мы выбрали для вас лучшие посты из аккаунта <strong>@<?= $user->username; ?></strong>.</li>
                        <li> Вы можете убрать любые из них, мы автоматически подберем другие.</li>
                        <li> Фотографии в книге будут установлены в хронологическом порядке - от старых к новым.
                        </li>
                    </ul>
                </div>
            </div>
            <div class='t396__elem tn-elem tn-elem__481710821519838086690' data-elem-id='1519838086690' style="top: 150px;">
                <div class='tn-atom'>
                    <div class="t603__container t-container_100">
                    </div>
                </div>
            </div>
            <div class='t396__elem tn-elem tn-elem__481710821519838164839' data-elem-id='1519838164839'>
                <div class='tn-atom'>

                </div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48171082');", \yii\web\View::POS_READY); ?>
</div>
<div id="rec48171083" class="r t-rec" style="display:none;" data-animationappear="off" data-record-type="396">
    <div class='t396'>
        <div class="t396__artboard" data-artboard-recid="48171083" data-artboard-height="160"
             data-artboard-height_vh="" data-artboard-valign="center">
            <div class="t396__carrier" data-artboard-recid="48171083"></div>
            <div class="t396__filter" data-artboard-recid="48171083"></div>
            <div class='t396__elem tn-elem tn-elem__481710831519838532963' data-elem-id='1519838532963'>
                <div class='tn-atom'>Создать мою книгу</div>
            </div>
        </div>
    </div>
    <?php $this->registerJs(/** @lang JavaScript */
        "t396_init('48171083');", \yii\web\View::POS_READY); ?>
</div>

<?php $this->registerJs(/** @lang JavaScript */"
$('.get-images').on('click', function() {
  var dateFrom = $('#dateFrom').val();
  var dateTo = $('#dateTo').val();
  var userId = $('#userId').val();
  var this_element = $(this);

  $.ajax({
  url:'/site/get-media?dateFrom=' + dateFrom + '&dateTo=' + dateTo + '&userId=' + userId,
  dataType: 'json',
  'beforeSend': function() {
    $('.cssload-container').show();
  },
  success: function(data) {
      if(data['success'] == true) {
          $.each(data['data'], function( index, value ) {
              $('.t-container_100').append('<div class=\"t603__tile t603__tile_25\">' +
               '<div class=\"t603__blockimg t603__blockimg_4-3 t-bgimg t-animate\"' +
                'data-animate-style=\"fadein\"' +
                 'data-animate-chain=\"yes\" bgimgfield=\"gi_img__0\"' +
                  'data-zoom-target=\"0\" data-zoomable=\"yes\"' +
                   'data-img-zoom-url=\"' + value['url'] + '\"' +
                    'data-original=\"' + value['url'] + '\"' +
                     'style=\"opacity:1;background: url(\'' + value['url'] + '\') center center / cover no-repeat;}\">' +
                      '<meta itemprop=\"image\" content=\"'+ value['url'] +'\">' +
                       '</div>' +
                        '</div>');
});
          $('.cssload-container').hide();
          $('#rec48171082').show();
var scrollTop = $('#rec48171082').offset().top;
console.log(scrollTop);
$('html, body').animate({scrollTop:scrollTop}, 'slow');


          
      }
  }
  });
  
  return false;
});
", \yii\web\View::POS_READY); ?>

<div class="cssload-container" style="display:none;">
    <div class="cssload-whirlpool"></div>
</div>
