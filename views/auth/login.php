<?php /** @var \app\forms\LoginForm $model */ ?>
<?php $this->title = 'Авторизация'; ?>
<?php $form = yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-signin'],
    'fieldConfig' => [
        'template' => "{label}{input}{error}",
        'labelOptions' => ['class' => 'sr-only'],
        'inputOptions' => ['class' => 'form-control'],
    ],
]); ?>
<h4 class="form-signin-heading"><?= yii\helpers\Html::encode($this->title); ?></h4>
<?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'test@test.test']]); ?>
<?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => 'password']])->passwordInput(); ?>
<div class="row">
    <div class="col-md-6">
        <?= yii\helpers\Html::submitButton('Войти', ['class' => 'btn btn-default btn-sm', 'name' => 'login-button']); ?>
    </div>
    <div class="col-md-6 text-right">
        <div class="checkbox">
            <?= $form->field($model, 'rememberMe')->checkbox(['template' => "{input}{label}"]); ?>
        </div>
    </div>
</div>


<?php yii\bootstrap\ActiveForm::end(); ?>
