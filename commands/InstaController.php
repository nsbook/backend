<?php

namespace app\commands;

use app\models\InstaAccount;
use app\models\InstaData;
use yii\console\Controller;

class InstaController extends Controller
{

    protected $user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.36 Safari/525.19',
        'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10',
        'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4',
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4',
        'Mozilla/5.0 (Linux; U; Android-4.0.3; en-us; Galaxy Nexus Build/IML74K) AppleWebKit/535.7 (KHTML, like Gecko) CrMo/16.0.912.75 Mobile Safari/535.7',
        'Mozilla/5.0 (Linux; Android 4.0.4; SGH-I777 Build/Task650 & Ktoonsez AOKP) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19',
        'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.',
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b3) Gecko/20090305 Firefox/3.1b3 GTB5',
        'Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.9b5) Gecko/2008032620 Firefox/3.0b5',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2) Gecko/20100308 Ubuntu/10.04 (lucid) Firefox/3.6 GTB7.1',
        'Mozilla/5.0 (Android; Linux armv7l; rv:9.0) Gecko/20111216 Firefox/9.0 Fennec/9.0',
        'Mozilla/4.0 (compatible; MSIE 5.0; Windows NT 4.0) Opera 6.01 [en]'
    ];

    /**
     * @throws \InstagramScraper\Exception\InstagramException
     */
    public function actionIndex()
    {
        /** @var InstaAccount[] $instaAccounts */
        $instaAccounts = InstaAccount::find()
            ->where(['is_active' => false])
            ->all();

        $i = 0;

        foreach ($instaAccounts as $instaAccount) {
            $limit = 50;
            $instagram = new \InstagramScraper\Instagram();
            $k = array_rand($this->user_agents);
            $instagram->setUserAgent($this->user_agents[$k]);
            $lastId = null;
            while ($limit <= $instaAccount->media_count) {

                $media = $instagram->getMediasByUserId($instaAccount->insta_id, $limit, $lastId);

                foreach ($media as $image) {
                    if ($image->getType() == 'image') {
                        try {
                            $instaData = new InstaData();
                            $instaData->insta_id = $instaAccount->insta_id;
                            $instaData->media_id = $image->getId();
                            $instaData->short_code = $image->getShortCode();
                            $instaData->caption = $image->getCaption();
                            $instaData->created_at = $image->getCreatedTime();
                            $instaData->url = $image->getImageHighResolutionUrl();
                            $instaData->likes_count = $image->getLikesCount();
                            $instaData->comments_count = $image->getCommentsCount();
                            $instaData->location_name = $image->getLocationName();
                            $instaData->save();

                        } catch (\yii\mongodb\Exception $e) {
                            // echo $e->getMessage();
                        } finally {
                            $lastId = $image->getId();
                        }
                    }
                }

                $i += $limit;
                sleep(1);
            }

            $instaAccount->is_active = true;
            $instaAccount->save();
        }

    }
}