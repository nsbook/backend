<?php

namespace app\commands;

use app\models\User;
use yii;

class UserController extends yii\console\Controller
{

    public function actionIndex()
    {
        /** @var User[] $users */
        $users = User::find()->all();
        if (count($users) == 0) {
            echo "Users Not found\n";
        } else {
            foreach ($users as $user) {
                echo sprintf("%s - ", $user->email) . "\n";
            }
        }
    }

    public function actionCreateSuperUser($email = 'test@test.com', $password = 'test')
    {
        try {
            $user = new User();
            $user->email = $email;
            $user->setPassword($password);
            $user->generateAuthKey();
            if (!$user->save()) {
                throw new yii\base\Exception("Error save user: " . print_r($user->errors, true));
            }
            echo "User create success.\n";
        } catch (\Exception $e) {
            echo 'Error: ', $e->getMessage(), "\n";
        }

    }
}