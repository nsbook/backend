<?php

namespace app\commands;

use app\components\ConsoleController;
use app\models\Order;
use Dompdf\Exception;

class PdfController extends ConsoleController
{

    public function actionIndex()
    {

        while (true) {

            /** @var Order[] $users */
            $users = Order::find()->all();

            foreach ($users as $user) {
                echo sprintf("%s - ", $user->_id) . "\n";
            }

            if (null === $users) {
                try {
                    $this->wait(20);
                } catch (Exception $e) {
                    break;
                }
                continue;
            }
        }
    }
}