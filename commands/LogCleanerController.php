<?php

namespace app\commands;

use app\models\Log;
use yii;

class LogCleanerController extends yii\console\Controller
{

    public function actionIndex()
    {
        Log::deleteAll([]);
    }

}