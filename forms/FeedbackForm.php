<?php

namespace app\forms;

use app\models\Feedback;
use yii\base\Model;

class FeedbackForm extends Model
{
    public $type;
    public $title;
    public $content;
    public $verifyCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title', 'content', 'verifyCode', 'type'], 'required'],
            ['type', 'in', 'range' => array_keys(Feedback::$types)],
            ['title', 'string', 'length' => [4, 64]],
            ['content', 'string', 'length' => [4, 256]],
            ['verifyCode', 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип обращения',
            'title' => 'Заголовок',
            'content' => 'Обращение',
            'verifyCode' => 'Код проверки',
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $feedback = new Feedback();
            $feedback->setAttributes(
                [
                    'type' => (int)$this->type,
                    'title' => (string)$this->title,
                    'content' => (string)$this->content,
                    'is_was_read' => false
                ]);
            $feedback->save();
            return true;
        }
        return false;
    }
}
