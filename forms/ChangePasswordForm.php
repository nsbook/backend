<?php

namespace app\forms;

use app\models\User;
use Yii;
use yii\base\Model;


class ChangePasswordForm extends Model
{
    public $password;
    public $passwordConfirm;


    public function rules()
    {
        return [
            [['password', 'passwordConfirm'], 'required'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function savePassword()
    {
        /** @var User $user */
        $user = User::find()->where(
            ['email' => Yii::$app->user->identity->email]
        )->one();
        $password = Yii::$app->security->generatePasswordHash($this->password);
        $user->password_hash = $password;
        $user->save();
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'passwordConfirm' => 'Повторите новый пароль',
        ];
    }
}