<?php

namespace app\forms;

use yii\base\Model;

class OrderForm extends Model
{
    public $login;
    public $user_id;
    public $post_count;
    public $email;
    public $phone;

    public $input_attributes = [
        'login' => [
            'id' => 'insta_login',
            'class' => 'form-control',
            'aria-describedby' => 'basic-addon1',
            'placeholder' => 'username',
            'autofocus' => true
        ]
    ];

    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [];
    }

}
