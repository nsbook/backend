<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * Contact model
 *
 * @property ObjectID $_id
 * @property string $content
 */
class Contact extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'content'
        ];
    }

    public function rules()
    {
        return [[['content'], 'required']];
    }

    public function attributeLabels()
    {
        return ['content' => 'Адрес'];
    }
}