<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * PostPrice model
 *
 * @property ObjectID $_id
 * @property int $count
 * @property int $price
 * @property boolean $is_visible
 */
class PostPrice extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'count',
            'price',
            'is_visible',
        ];
    }

    public function beforeSave($insert)
    {
        $this->count = (int)$this->count;
        $this->price = (int)$this->price;
        $this->is_visible = (bool)$this->is_visible;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['count', 'price'], 'required'],
            ['is_visible', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'count' => 'Колличество',
            'price' => 'Цена',
            'is_visible' => 'Отображать',
        ];
    }
}