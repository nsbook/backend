<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 *  InstaAccount model
 *
 * @property ObjectID $_id
 * @property int $insta_id
 * @property string $username
 * @property string $full_name
 * @property string $profile_pic_url
 * @property string $biography
 * @property int $follows_count
 * @property int $followed_by_count
 * @property int $media_count
 * @property bool $is_active
 */
class InstaAccount extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'insta_id',
            'username',
            'full_name',
            'profile_pic_url',
            'biography',
            'follows_count',
            'followed_by_count',
            'media_count',
            'is_active',
        ];
    }

    public function beforeSave($insert)
    {
        $this->insta_id = (int)$this->insta_id;
        $this->username = (string)$this->username;
        $this->full_name = (string)$this->full_name;
        $this->profile_pic_url = (string)$this->profile_pic_url;
        $this->biography = (string)$this->biography;
        $this->follows_count = (int)$this->follows_count;
        $this->followed_by_count = (int)$this->followed_by_count;
        $this->media_count = (int)$this->media_count;
        $this->is_active = (bool)$this->is_active;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [];
    }
}