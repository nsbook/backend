<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use yii\mongodb\ActiveRecord;

/**
 * Feedback model
 *
 * @property ObjectID $_id
 * @property int $type
 * @property string $title
 * @property string $content
 * @property UTCDateTime $created_at
 * @property boolean $is_was_read
 */
class Feedback extends ActiveRecord
{

    const TYPE_IDEA = 10;
    const TYPE_PROBLEM = 100;
    const TYPE_QUESTION = 1000;

    /** @var array $types */
    public static $types = [
        self::TYPE_IDEA => 'Идея',
        self::TYPE_PROBLEM => 'Проблема',
        self::TYPE_QUESTION => 'Вопрос',
    ];

    public function attributes()
    {
        return [
            '_id',
            'type',
            'title',
            'content',
            'created_at',
            'is_was_read',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = new UTCDateTime(time() * 1000);
            $this->is_was_read = false;
        }
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['title', 'content', 'type'], 'required'],
            ['type', 'in', 'range' => array_keys(Feedback::$types)],
            ['title', 'string', 'length' => [4, 64]],
            ['content', 'string', 'length' => [4, 256]],
            ['is_was_read', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип обращения',
            'title' => 'Заголовок',
            'content' => 'Сообщение',
            'created_at' => 'Дата обращения',
            'is_was_read' => 'Прочитано',
        ];
    }
}