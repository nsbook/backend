<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * Client model
 *
 * @property ObjectID $_id
 * @property string $name
 * @property string $mail
 * @property string $phone
 * @property boolean $is_new
 */
class Client extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'name',
            'mail',
            'phone',
            'is_new',
        ];
    }

    public function beforeSave($insert)
    {
        $this->name = (int)$this->name;
        $this->mail = (string)$this->mail;
        $this->phone = (string)$this->phone;
        $this->is_new = (bool)$this->is_new;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['name', 'mail', 'phone'], 'required'],
            ['is_new', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'mail' => 'Почта',
            'phone' => 'Телефон',
            'is_new' => 'Новый',
        ];
    }
}