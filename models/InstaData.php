<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 *  InstaData model
 *
 * @property ObjectID $_id
 * @property int $insta_id
 * @property string $media_id
 * @property string $short_code
 * @property string $caption
 * @property int $created_at
 * @property string $url
 * @property int $likes_count
 * @property int $comments_count
 * @property string $location_name
 *
 */
class InstaData extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'insta_id',
            'media_id',
            'short_code',
            'caption',
            'created_at',
            'url',
            'likes_count',
            'comments_count',
            'location_name',
        ];
    }

    public function beforeSave($insert)
    {
        $this->insta_id = (int)$this->insta_id;
        $this->media_id = (string)$this->media_id;
        $this->short_code = (string)$this->short_code;
        $this->caption = (string)$this->caption;
        $this->created_at = (int) $this->created_at;
        $this->url = (string)$this->url;
        $this->likes_count = (int)$this->likes_count;
        $this->comments_count = (int)$this->comments_count;
        $this->location_name = (string)$this->location_name;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'mail' => 'Почта',
            'phone' => 'Телефон',
            'is_new' => 'Новый',
        ];
    }
}