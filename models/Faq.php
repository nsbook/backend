<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * Faq model
 *
 * @property ObjectID $_id
 * @property int $category
 * @property string $question
 * @property string $answer
 * @property boolean $is_visible
 */
class Faq extends ActiveRecord
{

    const CATEGORY_ABOUT_BOOK = 10;
    const CATEGORY_ORDER_AND_DELIVERY = 100;
    const CATEGORY_TECHNOLOGY = 1000;

    /** @var array $categories */
    public static $categories = [
        self::CATEGORY_ABOUT_BOOK => 'Часто задаваемые вопросы',
        self::CATEGORY_ORDER_AND_DELIVERY => 'Оплата и доставка',
        self::CATEGORY_TECHNOLOGY => 'Технологии',
    ];

    public function attributes()
    {
        return [
            '_id',
            'category',
            'question',
            'answer',
            'is_visible',
        ];
    }

    public function beforeSave($insert)
    {
        $this->category = (int)$this->category;
        $this->question = (string)$this->question;
        $this->answer = (string)$this->answer;
        $this->is_visible = (bool)$this->is_visible;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['category', 'question', 'answer'], 'required'],
            ['category', 'in', 'range' => array_keys(self::$categories)],
            ['is_visible', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'category' => 'Категория',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'is_visible' => 'Отображать',
        ];
    }
}