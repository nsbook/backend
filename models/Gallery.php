<?php

namespace app\models;

use app\components\SortableGridBehavior;
use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * Gallery model
 *
 * @property ObjectID $_id
 * @property string $file
 * @property int $priority
 */
class Gallery extends ActiveRecord
{

    public function attributes()
    {
        return [
            '_id',
            'file',
            'priority'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->priority = 0;
        }

        $this->file = (string)$this->file;
        $this->priority = (int)$this->priority;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['file'], 'required'],
            ['priority', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'priority' => 'Сортировка',
        ];
    }

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::class,
                'sortableAttribute' => 'priority'
            ],
        ];
    }

    public function geturl()
    {
        return \Yii::getAlias('@web') . '/gallery/' . $this->file;
    }

    public function afterDelete()
    {
        $directory = \Yii::getAlias('@webroot/gallery');
        if (is_file($directory . DIRECTORY_SEPARATOR . $this->file)) {
            unlink($directory . DIRECTORY_SEPARATOR . $this->file);
        }
        parent::afterDelete();
    }
}