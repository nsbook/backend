<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use yii\log\Logger;
use yii\mongodb\ActiveRecord;

/**
 * Log model
 *
 * @property ObjectID $_id
 * @property int $level
 * @property string $category
 * @property UTCDateTime $log_time
 * @property string $message
 */
class Log extends ActiveRecord
{


    /** @var array $levels */
    public static $levels = [
        Logger::LEVEL_ERROR => 'Ошибка',
        Logger::LEVEL_WARNING => 'Предупреждение',
        Logger::LEVEL_INFO => 'Информация',
        Logger::LEVEL_TRACE => 'Троссировка',
    ];

    public function attributes()
    {
        return [
            '_id',
            'level',
            'category',
            'log_time',
            'message',
        ];
    }

    public function attributeLabels()
    {
        return [
            'level' => 'Уровень',
            'category' => 'Категория',
            'log_time' => 'Дата и время',
            'message' => 'Соообщение',
        ];
    }
}