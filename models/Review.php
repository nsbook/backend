<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use yii\mongodb\ActiveRecord;

/**
 * Review model
 *
 * @property ObjectID $_id
 * @property string $title
 * @property string $file
 * @property string $content
 * @property boolean $is_visible
 */
class Review extends ActiveRecord
{


    public function attributes()
    {
        return [
            '_id',
            'title',
            'file',
            'content',
            'is_visible',
        ];
    }

    public function beforeSave($insert)
    {
        $this->title = (string)$this->title;
        $this->content = (string)$this->content;
        $this->is_visible = (bool)$this->is_visible;
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            ['is_visible', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовк',
            'file' => 'Фото',
            'content' => 'Текст',
            'is_visible' => 'Отображать',
        ];
    }

    public function geturl()
    {
        return \Yii::getAlias('@web') . '/review/' . $this->file;
    }

    public function afterDelete()
    {
        $directory = \Yii::getAlias('@webroot/review');
        if (is_file($directory . DIRECTORY_SEPARATOR . $this->file)) {
            unlink($directory . DIRECTORY_SEPARATOR . $this->file);
        }
        parent::afterDelete();
    }
}