<?php

namespace app\models;

use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDateTime;
use yii\mongodb\ActiveRecord;

/**
 * Order model
 *
 * @property ObjectID $_id
 * @property UTCDateTime $created_at
 * @property int $type
 * @property int $status
 * @property string $client_id
 * @property string $post_price_id
 * @property string $instagram
 * @property array $data
 * @property boolean $order_likes
 * @property boolean $use_carousel
 * @property boolean $use_text
 * @property boolean $use_geo
 * @property boolean $use_date
 * @property boolean $use_likes
 *
 * @property PostPrice $postPrice
 * @property Client $client
 */
class Order extends ActiveRecord
{

    const TYPE_INSTA = 10;
    const TYPE_PHOTO = 100;

    const STATUS_NEW = 10;
    const STATUS_WORK = 100;
    const STATUS_PAID = 1000;
    const STATUS_CANCEL = 10000;
    const STATUS_SUCCESS = 100000;

    /** @var array $types */
    public static $types = [
        self::TYPE_INSTA => 'Фото из инстаграма',
        self::TYPE_PHOTO => 'Загруженные фото',
    ];

    /** @var array $statuses */
    public static $statuses = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_WORK => 'В работе',
        self::STATUS_PAID => 'Оплачен',
        self::STATUS_CANCEL => 'Отменен',
        self::STATUS_SUCCESS => 'Выполнен',
    ];

    public function attributes()
    {
        return [
            '_id',
            'type',
            'status',
            'client_id',
            'post_price_id',
            'instagram',
            'data',
            'order_likes',
            'use_carousel',
            'use_text',
            'use_geo',
            'use_date',
            'use_likes',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = new UTCDateTime(time() * 1000);
        }
        return parent::beforeSave($insert);

    }

    public function getClient()
    {
        return $this->hasOne(Client::class, ['client_id' => '_id']);
    }

    public function getPostPrice()
    {
        return $this->hasMany(PostPrice::class, ['post_price_id' => '_id']);
    }

    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            '_id' => 'ID заказы',
            'type' => 'Тип заказы',
            'status' => 'Статус заказа',
            'client_id' => 'Клиент',
            'post_price_id' => 'Колличество постов',
            'instagram' => 'Аккаунт инстаграм',
            'data' => 'Данные',
            'order_likes' => 'Выбирать посты с наибольшим количеством лайков',
            'use_carousel' => 'Использовать все фотографии из поста (карусели)',
            'use_text' => 'Печатать подпись постов',
            'use_geo' => 'Печатать геоданные',
            'use_date' => 'Печатать дату постов',
            'use_likes' => 'Печатать количество лайков',
        ];
    }
}