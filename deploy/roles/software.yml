---

- name: install system packages
  apt: name={{ item }}
  with_items:
    - openssl
    - libssl-dev
    - libcurl4-openssl-dev
    - curl

- name: install git
  apt:
    name: git

- name: Update apt cache
  apt:
    update_cache: yes

- name: Install required packages
  apt:
    name: nginx
    install_recommends: yes

- name: Start nginx service
  service:
    name: nginx
    state: started

- name: install memcached
  apt:
    name: memcached

- name: memcached started...
  shell: systemctl start memcached

- name: mongodb (add PPA key)
  apt_key: >
    keyserver=hkp://keyserver.ubuntu.com:80
    id=2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
    state=present

- name: mongodb (add to sources list)
  lineinfile: >
    line="deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse"
    dest=/etc/apt/sources.list.d/mongodb-org-3.6.list
    state=present
    create=yes

- name: mongodb (install packages)
  apt: name={{ item }}
  with_items:
    - mongodb-org
    - mongodb-org-mongos
    - mongodb-org-server
    - mongodb-org-shell
    - mongodb-org-tools

- name: mongodb (create service file)
  template: src=templates/mongodb.service.j2 dest=/etc/systemd/system/mongodb.service

- name: mongodb started...
  shell: systemctl start mongodb

- name: install php
  apt: name={{ item }}
  with_items:
    - php-pear
    - php-mongodb
    - php7.0-json
    - php7.0-readline
    - php7.0-fpm
    - php7.0-curl
    - php7.0-mcrypt
    - php7.0-dev
    - php7.0-mbstring
    - php7.0-zip
    - php-imagick
    - php-memcache

- name: install PECL modules

  pear: name={{ item }}
  with_items:
    - pecl/mongodb
  notify:
    - reload php-fpm

- name: enable php mods
  command: /usr/sbin/phpenmod {{ item }}
  register: phpenmod
  with_items:
    - json
    - mongodb
    - curl
    - mcrypt
  changed_when: false
  failed_when: phpenmod.stdout.find('WARNING') != -1

- name: install composer
  shell: curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin creates=/usr/local/bin/composer

- name: rename composer.phar to composer
  shell: mv /usr/local/bin/composer.phar /usr/local/bin/composer creates=/usr/local/bin/composer

- name: make composer executable
  file: path=/usr/local/bin/composer mode=a+x state=file
  tags:
    - filestore
    - composer

- name: update Composer to latest version
  shell: >
    /usr/local/bin/composer self-update
  register: composer_update
  changed_when:
    composer_update.stdout.lower().find('updating to version') != -1
    or composer_update.stderr.lower().find('updating to version') != -1

- name: install fxp/composer-asset-plugin
  command: composer global require "fxp/composer-asset-plugin:*"
  register: composer_asset_plugin
  changed_when: not (composer_asset_plugin.stdout.find('Nothing to install or update') == -1
              or composer_asset_plugin.stderr.find('Nothing to install or update') == -1)
  failed_when: composer_asset_plugin.stdout.find('Exception') != -1
              or composer_asset_plugin.stderr.find('Exception') != -1