<?php

namespace app\modules\manage\controllers;

use app\forms\ChangePasswordForm;
use yii\filters\AccessControl;
use yii\web\Controller;

class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['ChangePassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['/manage/ChangePassword']);
    }

    public function actionChangePassword()
    {
        $model = new ChangePasswordForm();
        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->savePassword();
                \Yii::$app->session->setFlash('success', 'Пароль успешно сохранен!');
                return $this->redirect(['/manage/default']);
            }
        }
        return $this->render('change_password', ['model' => $model]);
    }
}
