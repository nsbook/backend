<?php

namespace app\modules\manage\controllers;

use app\models\Client;
use app\models\Cover;
use app\models\Faq;
use app\models\Feedback;
use app\models\Gallery;
use app\models\Log;
use app\models\Order;
use app\models\PostPrice;
use app\models\Review;
use yii\filters\AccessControl;
use yii\web\Controller;


class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index', 'ChangePassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $logCount = Log::find()->count();

        $feedbackCount = Feedback::find()->count();
        $feedbackCountNotWasRead = count(Feedback::findAll(['is_was_read' => false]));

        $faqCount = Faq::find()->count();
        $faqCountIsNotVisible = count(Faq::findAll(['is_visible' => false]));

        $galleryCount = Gallery::find()->count();

        $coverCount = Cover::find()->count();

        $clientCount = Client::find()->count();
        $clientCountIsNew = count(Client::findAll(['is_new' => true]));

        $orderCount = Order::find()->count();
        $orderCountIsNew = count(Order::findAll(['status' => Order::STATUS_NEW]));

        $postPriceCount = PostPrice::find()->count();
        $postPriceCountIsNotVisible = count(PostPrice::findAll(['is_visible' => false]));

        $reviewCount = Review::find()->count();
        $reviewCountIsNotVisible = count(Review::findAll(['is_visible' => false]));

        $data = [
            'Логи (Всего)' => [
                'all' => $logCount,
                'notAll' => null,
                'url' => ['manage/log/index']
            ],
            'Обратная связь (Не прочитано / Всего)' => [
                'all' => $feedbackCount,
                'notAll' => $feedbackCountNotWasRead,
                'url' => ['manage/feedback/index']
            ],
            'FAQ (Не отображается / Всего)' => [
                'all' => $faqCount,
                'notAll' => $faqCountIsNotVisible,
                'url' => ['manage/faq/index']
            ],
            'Галерея (Всего)' => [
                'all' => $galleryCount,
                'notAll' => null,
                'url' => ['manage/gallery/index']
            ],
            'Отзывы (Не отображается / Всего)' => [
                'all' => $reviewCount,
                'notAll' => $reviewCountIsNotVisible,
                'url' => ['manage/review/index']
            ],

            'Обложки (Всего)' => [
                'all' => $coverCount,
                'notAll' => null,
                'url' => ['manage/cover/index']
            ],
            'Посты и цены (Не отображается / Всего)' => [
                'all' => $postPriceCount,
                'notAll' => $postPriceCountIsNotVisible,
                'url' => ['manage/post-price/index']
            ],
            'Клиенты (Новые / Всего)' => [
                'all' => $clientCount,
                'notAll' => $clientCountIsNew,
                'url' => ['manage/client/index']
            ],
            'Заказы (Новые / Всего)' => [
                'all' => $orderCount,
                'notAll' => $orderCountIsNew,
                'url' => ['manage/order/index']
            ]
        ];

        return $this->render('index', ['data' => $data]);
    }
}
