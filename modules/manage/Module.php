<?php

namespace app\modules\manage;


class Module extends \yii\base\Module
{

    public $controllerNamespace = 'app\modules\manage\controllers';

    public $layout = '//manage';

    public function init()
    {
        parent::init();
    }
}
