<?php

namespace app\modules\manage\models;

use app\models\Client;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ClientSearch extends Client
{

    public function rules()
    {
        return [
            [['name', 'mail', 'phone', 'is_new'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['name' => SORT_ASC]]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->is_new) {
            $query->andWhere(['is_new' => (bool)$this->is_new]);
        }

        $query->andFilterWhere(['like', 'name', (string)$this->name])
            ->andFilterWhere(['like', 'mail', (string)$this->name])
            ->andFilterWhere(['like', 'phone', (string)$this->phone]);

        return $dataProvider;
    }
}
