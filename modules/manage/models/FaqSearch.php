<?php

namespace app\modules\manage\models;

use app\models\Faq;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class FaqSearch extends Faq
{
    public function rules()
    {
        return [
            [['_id', 'category', 'question', 'answer', 'is_visible'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Faq::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['category' => SORT_ASC]]]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        if ($this->category) {
            $query->andWhere(['category' => (int)$this->category]);
        }

        if ($this->is_visible) {
            $query->andWhere(['is_visible' => (bool)$this->is_visible]);
        }

        $query->andFilterWhere(['like', 'question', $this->question])->andFilterWhere(['like', 'answer', $this->answer]);

        return $dataProvider;
    }
}
