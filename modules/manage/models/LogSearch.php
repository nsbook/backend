<?php

namespace app\modules\manage\models;

use app\models\Log;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class LogSearch extends Log
{
    public function rules()
    {
        return [
            ['level', 'in', 'range' => array_keys(Log::$levels)],
            [['message'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Log::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['log_time' => SORT_DESC]]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->level) {
            $query->andWhere(['level' => (int)$this->level]);
        }

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
