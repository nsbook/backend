<?php

namespace app\modules\manage\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedback;
use yii\helpers\VarDumper;


class FeedbackSearch extends Feedback
{

    public function rules()
    {
        return [
            [['type', 'title', 'content', 'created_at', 'is_was_read'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder'=>['created_at' => SORT_DESC]]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->type){
            $query->andWhere(['type' => (int) $this->type]);
        }

        if ($this->is_was_read){
            $query->andWhere(['is_was_read' => (bool) $this->is_was_read]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}
