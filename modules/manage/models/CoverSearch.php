<?php

namespace app\modules\manage\models;

use app\models\Cover;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CoverSearch extends Cover
{
    public function rules()
    {
        return [
            [['_id', 'file', 'priority'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Cover::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['priority' => SORT_ASC]]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
