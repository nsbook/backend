<?php

namespace app\modules\manage\models;

use app\models\Contact;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ContactSearch extends Contact
{

    public function rules()
    {
        return [
            [['content'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Contact::find();

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'content', (string)$this->content]);

        return $dataProvider;
    }
}
