<?php

namespace app\modules\manage\models;

use app\models\Review;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ReviewSearch extends Review
{

    public function rules()
    {
        return [
            [['title', 'is_visible'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['title' => SORT_ASC]]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->is_visible) {
            $query->andWhere(['is_new' => (bool)$this->is_visible]);
        }

        $query->andFilterWhere(['like', 'title', (string)$this->title]);

        return $dataProvider;
    }
}
