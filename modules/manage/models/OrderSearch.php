<?php

namespace app\modules\manage\models;

use app\models\Order;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class OrderSearch extends Order
{

    public function rules()
    {
        return [
            [['type', 'status'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder'=>['is_new']]]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

//        if ($this->type){
//            $query->andWhere(['type' => (int) $this->type]);
//        }
//
//        if ($this->is_was_read){
//            $query->andWhere(['is_was_read' => (bool) $this->is_was_read]);
//        }
//
//        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'content', $this->content])
//            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}
