<?php

namespace app\modules\manage\models;

use app\models\PostPrice;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PostPriceSearch extends PostPrice
{
    public function rules()
    {
        return [
            [['_id', 'count', 'price', 'is_visible'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PostPrice::find();

        $dataProvider = new ActiveDataProvider(['query' => $query, 'sort' => ['defaultOrder' => ['count' => SORT_ASC]]]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        if ($this->count) {
            $query->andWhere(['count' => (int)$this->count]);
        }

        if ($this->price) {
            $query->andWhere(['price' => (int)$this->price]);
        }

        if ($this->is_visible) {
            $query->andWhere(['is_visible' => (bool)$this->is_visible]);
        }

        return $dataProvider;
    }
}
