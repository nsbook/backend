<?php

namespace app\modules\manage\models;

use app\models\Video;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class VideoSearch extends Video
{

    public function rules()
    {
        return [
            [['content'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Video::find();

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'content', (string)$this->content]);

        return $dataProvider;
    }
}
