<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Review */
$this->title = "{$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
$this->title  = "Редактировать " . $this->title;
?>
<div class="gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
