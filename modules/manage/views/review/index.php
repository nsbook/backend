<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/** @var array $categories */
$categories = \app\models\Faq::$categories;
$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить Отзыв', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'file',
                'format' => 'html',
                'filter' => false,
                'value' => function ($data) {
                    /** @var \app\models\Review $data */
                    return Html::img($data->file, ['height' => '100px']);
                },
            ],
            [
                'attribute' => 'content',
                'format' => 'html',
                'filter' => false,
                'value' => function ($data) {
                    /** @var \app\models\Review $data */
                    return $data->content;
                },
            ],
            'is_visible:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
