<?php

use yii\helpers\Html;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
/* @var $form ActiveForm */
?>
<div class="form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title'); ?>
    <?= dosamigos\fileupload\FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'file',
        'url' => ['review/upload', 'id' => $model->_id],
        'gallery' => true,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 2000000,
            'maxNumberOfFiles' => 1,
        ],
    ]); ?>
    <?= $form->field($model, 'content')->widget(Redactor::class); ?>
    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>


</div>