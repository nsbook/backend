<?php $this->title = "Главная"; ?>
<div class="manage-default-index">
    <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($data as $key => $value): ?>
            <div class="col-md-4">
                <a class="admin-block-url" href="<?= Yii::$app->urlManager->createUrl($value['url']); ?>">
                    <div class="thumbnail">
                        <div class="caption">
                            <?php if (is_null($value['notAll'])): ?>
                                <h3 class="text-center"><?= $value['all']; ?></h3>
                            <?php else: ?>
                                <h3 class="text-center"><?= $value['notAll']; ?> / <?= $value['all']; ?></h3>
                            <?php endif; ?>
                            <p class="text-center"><?= $key; ?></p>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
