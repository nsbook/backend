<?php

use app\models\Feedback;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обратная связь';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="feedback-index">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'id' => 'feedback-gridview',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'type',
                    'filter' => Feedback::$types,
                    'value' => function ($model) {
                        /** @var Feedback $model */
                        return Feedback::$types[$model->type];
                    }
                ],
                'title',
                'content',
                [
                    'attribute' => 'created_at',
                    'value' => function ($model) {
                        /** @var Feedback $model */
                        return $model->created_at->toDateTime()->format('Y-m-d H:i:s');
                    }
                ],
                [
                    'attribute' => 'is_was_read',
                    'filter' => [0 => 'Нет', 1 => 'Да'],
                    'format' => 'raw',
                    'value' => function ($model, $index) {
                        return Html::checkbox('', $model->is_was_read, ['value' => $index, 'data-id' => $model->_id]);
                    },
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
<?php $this->registerJs(
/** @lang JavaScript */
    "
$(':checkbox').on('change', function() {
    id = $(this).attr('data-id');
      $.ajax({
  url:'/manage/feedback/read?id=' + id,
  complete: function(data) {
      $.pjax({container: '#feedback-gridview'})
  }
  });
})
", \yii\web\View::POS_READY); ?>