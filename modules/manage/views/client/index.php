<?php

use app\models\Feedback;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="feedback-index">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'id' => 'client-gridview',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'mail',
                'phone',
                [
                    'attribute' => 'is_new',
                    'filter' => [0 => 'Нет', 1 => 'Да'],
                    'format' => 'raw',
                    'value' => function ($model, $index) {
                        return Html::checkbox('', $model->is_new, ['value' => $index, 'data-id' => $model->_id]);
                    },
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
<?php $this->registerJs(
/** @lang JavaScript */
    "
$(':checkbox').on('change', function() {
    id = $(this).attr('data-id');
      $.ajax({
  url:'/manage/client/read?id=' + id,
  complete: function(data) {
      $.pjax({container: '#client-gridview'})
  }
  });
})
", \yii\web\View::POS_READY); ?>