<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostPrice */

$this->title = "{$model->count} - {$model->price} (колличество постов - цена)";
$this->params['breadcrumbs'][] = ['label' => 'Посты и цены', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->title  = "Редактировать " . $this->title;
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
