<?php

use app\models\Order;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'id' => 'order-gridview',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            '_id',
            [
                'attribute' => 'type',
                'filter' => Order::$types,
                'value' => function ($model) {
                    /** @var Order $model */
                    return Order::$types[$model->type];
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Order::$statuses,
                'value' => function ($model) {
                    /** @var Order $model */
                    return Order::$statuses[$model->status];
                }
            ],
            [
                'attribute' => 'client_id',
                'filter' => false,
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->client->name;
                }
            ],
            [
                'attribute' => 'post_price_id',
                'filter' => false,
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->postPrice->price;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>