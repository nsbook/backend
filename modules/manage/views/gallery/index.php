<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Галерея';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="gallery-index">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?php if ($dataProvider->count < 20): ?>
                <?= Html::a('Добавить изображения', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
            <?php endif; ?>
            <button type="button" class="btn btn-info btn-sm" data-container="body" data-toggle="popover"
                    data-placement="top"
                    data-content="Чтобы отсортировать отображение изображений на сайте, просто перетащите его мышкой вверх или вниз.">
                Сортировка
            </button>
        </p>

        <?= himiklab\sortablegrid\SortableGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'file',
                    'format' => 'html',
                    'filter' => false,
                    'label' => 'Файл',
                    'value' => function ($data) {
                        /** @var \app\models\Gallery $data */
                        return Html::img($data->url, ['height' => '100px']);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}'
                ],
            ],
        ]); ?>
    </div>
<?php $this->registerJs(/** @lang JavaScript */
    "$('.btn-info').popover();", \yii\web\View::POS_READY); ?>