<?php
/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">
    <?php $form = yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= dosamigos\fileupload\FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'file',
        'url' => ['gallery/upload', 'id' => $model->_id],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 2000000,
            'maxNumberOfFiles' => 20,
        ],
    ]); ?>
    <?php yii\widgets\ActiveForm::end(); ?>

</div>
