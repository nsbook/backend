<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($dataProvider->count < 1): ?>
        <?= Html::a('Добавить Видео', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        <?php endif; ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'content',
                'format' => 'raw',
                'filter' => false,
                'label' => 'Видео',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
