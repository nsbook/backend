<?php

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = 'Добавить изображение';
$this->params['breadcrumbs'][] = ['label' => 'Обложки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">
    <h1><?= yii\helpers\Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
