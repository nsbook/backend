<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\CoverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обложки';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="gallery-index">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?= Html::a('Добавить изображения', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
            <button type="button" class="btn btn-info btn-sm" data-container="body" data-toggle="popover"
                    data-placement="top"
                    data-content="Чтобы отсортировать отображение изображений на сайте, просто перетащите его мышкой вверх или вниз.">
                Сортировка
            </button>
        </p>

        <?= himiklab\sortablegrid\SortableGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'file',
                    'format' => 'html',
                    'filter' => false,
                    'label' => 'Файл',
                    'value' => function ($data) {
                        /** @var \app\models\Cover $data */
                        return Html::img($data->url, ['height' => '100px']);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}'
                ],
            ],
        ]); ?>
    </div>
<?php $this->registerJs(/** @lang JavaScript */
    "$('.btn-info').popover();", \yii\web\View::POS_READY); ?>