<?php use yii\helpers\Html; ?>
<?php use yii\grid\GridView; ?>
<?php /* @var $this yii\web\View */ ?>
<?php /* @var $searchModel app\modules\manage\models\LogSearch */ ?>
<?php /* @var $dataProvider yii\data\ActiveDataProvider */ ?>
<?php $this->title = 'Логи'; ?>
<?php $this->params['breadcrumbs'][] = $this->title; ?>
<div class="log-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p class="hint-block">Логи очищаются раз в неделю</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'log_time',
                'format' => 'html',
                'filter' => false,
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return $data->log_time->toDateTime()->format('Y-m-d H:i:s');
                },
            ],
            [
                'attribute' => 'level',
                'format' => 'html',
                'filter' => \app\models\Log::$levels,
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return \app\models\Log::$levels[$data->level];
                },
            ],
            [
                'attribute' => 'message',
                'format' => 'html',
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return \yii\helpers\StringHelper::truncateWords($data->message, 10);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
