<?php use yii\helpers\Html; ?>
<?php use yii\widgets\DetailView; ?>
<?php /* @var $this yii\web\View */ ?>
<?php /* @var $model app\models\Log */ ?>
<?php $this->title = "{$model->_id}"; ?>
<?php $this->params['breadcrumbs'][] = ['label' => 'Логи', 'url' => ['index']]; ?>
<?php $this->params['breadcrumbs'][] = $this->title; ?>
<div class="log-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'log_time',
                'format' => 'html',
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return \Yii::$app->formatter->asDatetime($data->log_time, 'Y-m-d H:i:s');
                },
            ],
            [
                'attribute' => 'level',
                'format' => 'html',
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return \app\models\Log::$levels[$data->level];
                },
            ],
            'category',
            [
                'attribute' => 'message',
                'format' => 'html',
                'value' => function ($data) {
                    /** @var \app\models\Log $data */
                    return \yii\helpers\VarDumper::dumpAsString($data->message, 10, true);
                },
            ],
        ],
    ]) ?>

</div>
